# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.6.0] - 2024-09-24
### Added
- Support PostgreSQL 17 parameters.
- Add the Java module declaration `com.ongres.pgconfig.validator`.
- Add ITs for testing native-image generation.

### Changed
- Require Java 11 at runtime (goodbye Java 8).
- Use JSpecify Nullness Annotations.
- Bump dependencies and plugins versions.
- Improve native-image generation with `resource-config.json`.

## [1.5.0] - 2023-09-15
### Added
- Support PostgreSQL 16 parameters.
- Provide CycloneDX SBOM.

## [1.4.0] - 2022-10-26
### Added
- Support PostgreSQL 15 parameters.

## [1.3.0] - 2021-11-02
### Added
- Support PostgreSQL 14 parameters.

### Fixed
- Some parameters was not following Postgres formating, like "vacuum_cost_delay, 99.99ms -> 99990us".

### Changed
- Require Maven 3.8.3 and Java 17 to build the project, Java 8 is still the minimum required at runtime.
- Use `Locale.ROOT` explicitly on `String::format` and `String::toLowerCase`.
- Updated all the dependencies used by the project.
- Add `forbiddenapis` and `modernizer-maven-plugin` as additional checks at build time.
- Add Maven `checks` profile and move all static analyzers to that profile.

## [1.2.2] - 2020-11-27
### Fixed
- Property handle empty or zero values on integer parameters.
- Show correct padding of zeros on octal values.

## [1.2.1] - 2020-10-22
### Fixed
- Minus-one values has special meaning on parameters with units, so returned as-is.
- Parameters of type integer should return integer (rounded) values on real inputs.
- Return scientific notation if an exponent is needed on real values.

## [1.2.0] - 2020-10-02
### Added
- Support PostgreSQL 13 parameters.
- Integer parameters additionally accept hexadecimal input (beginning with 0x) and octal input (beginning with 0).
- Show customary octal format for `unix_socket_permissions` and `log_file_mode` parameters.

### Fixed
- Parameters like `DateStyle` and `TimeZone` should be returned with the default case.
- Custom parameters should not start with dot (.) in the name.

## [1.1.3] - 2020-08-28
### Changed
- Use `@ParameterizedTest` in some tests to improve reading and maintenance.
- Deploy flattened pom.xml with `flatten-maven-plugin`.

## [1.1.2] - 2020-04-22
### Fixed
- Fixed an issue with the Numeric with Unit parser when the Unit is before the number.

## [1.1.1] - 2020-04-13
### Fixed
- Previously, enum parameter values were case-sensitive, now case-insensitive values are accepted.

### Changed
- Use `NonNullByDefault` default locations.

## [1.1.0] - 2020-03-23
### Added
- Detection for custom parameters, now parameters with a dot are returned as valid.
- Use `org.eclipse.jdt.annotation` to help with the detection of `NullPointerException`.
- Add README.md with a small description of the project and how to use.

### Changed
- For parameters not found return a *unrecognized configuration parameter* error message.
- Improve Javadoc of PgParameter.getSetting() to make it clearer.
- Update CSV files to remove unused memory/time attribute.
- Improve code quality with a custom ruleset for PMD.

## [1.0.0] - 2020-03-17
### Added
- The *PostgreSQL Configuration Parameter Validator* gives you the ability to verify if a GUC parameter is valid.
- Initial stable API and implementation.
- Add `native-image.properties` to support GraalVM native compilation.

[Unreleased]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.6.0...master
[1.6.0]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.5.0...1.6.0
[1.5.0]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.4.0...1.5.0
[1.4.0]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.3.0...1.4.0
[1.3.0]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.2.2...1.3.0
[1.2.2]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.2.1...1.2.2
[1.2.1]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.1.3...1.2.0
[1.1.3]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.1.2...1.1.3
[1.1.2]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.1.1...1.1.2
[1.1.1]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.1.0...1.1.1
[1.1.0]: https://gitlab.com/ongresinc/pgconfig-validator/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/ongresinc/pgconfig-validator/-/tags/1.0.0
