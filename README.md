# Postgres Configuration Validator

> PostgreSQL Configuration Parameter Validator gives you the ability to verify if a GUC (Grand Unified Configuration) parameter is valid based on a few rules that are checked depending on the PostgreSQL server version being used.

## Installation

This library is published on Maven Central with GroupId `com.ongres.pgconfig` and ArtifactId `pgconfig-validator`:

[![Maven Central](https://img.shields.io/badge/maven--central-pgconfig--validator-informational?style=for-the-badge&logo=apache-maven&logoColor=red)](https://maven-badges.herokuapp.com/maven-central/com.ongres.pgconfig/pgconfig-validator)

## Usage

```java
GucValidator validator = GucValidator.forVersion("14");
PgParameter param = validator.parameter("work_mem", "1048576 MB");
assertEquals("work_mem", param.getName()); // Parameter name
assertEquals("1TB", param.getSetting()); // Parsed setting
assertTrue(param.isValid()); // Parameter is valid
assertEquals(Optional.empty(), param.getError()); // No error message
assertEquals(Optional.empty(), param.getHint()); // No hint message
```

The API aims to be very simple, the `GucValidator` is the entry point for the library, first load the version of PostgreSQL to validate the parameters, then load the parameter with the setting to validate, this will return a `PgParameter` instance with a few methods like `isValid()`, and if there is an error an `Optional<String>` is returned with the error message of the validation.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

```
Copyright 2020 OnGres, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
