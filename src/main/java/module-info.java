/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * Module providing validation utilities for PostgreSQL configuration parameters.
 *
 * <h2>Postgres Configuration Validator</h2>
 *
 * <p>
 * PostgreSQL Configuration Parameter Validator gives you the ability to verify if a GUC (Grand Unified Configuration) parameter
 * is valid based on a few rules that are checked depending on the PostgreSQL server version being used.
 * </p>
 *
 * <h3>Example</h3>
 * 
 * <pre>{@code
 * // Example of validating a PostgreSQL parameter
 * GucValidator validator = GucValidator.forVersion("17");
 * PgParameter param = validator.parameter("work_mem", "1048576 MB");
 * assertEquals("work_mem", param.getName()); // Parameter name
 * assertEquals("1TB", param.getSetting()); // Parsed setting
 * assertTrue(param.isValid()); // Parameter is valid
 * assertEquals(Optional.empty(), param.getError()); // No error message
 * assertEquals(Optional.empty(), param.getHint()); // No hint message
 * }</pre>
 *
 * <p>
 * The API aims to be very simple, the {@code GucValidator} is the entry point for the library,
 * first load the version of PostgreSQL to validate the parameters, then load the parameter
 * with the setting to validate, this will return a {@code PgParameter} instance with a few methods
 * like {@code isValid()}, and if there is an error an {@code Optional<String>} is returned with the
 * error message of the validation.
 * </p>
 *
 * @author Jorge Solórzano
 * @since 1.6.0
 */
module com.ongres.pgconfig.validator {
  exports com.ongres.pgconfig.validator;

  requires org.jspecify;
  requires static org.immutables.value.annotations;
  requires static org.immutables.builder;
}