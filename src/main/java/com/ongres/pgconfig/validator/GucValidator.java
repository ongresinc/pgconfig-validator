/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * Main Validator entry point. You can create an instance for an specific Postgres version and
 * validated all the GUC parameters.
 *
 * <p>Load the Postgres version and do the lookup of the parameter.</p>
 *
 * <pre>{@code
 *   GucValidator validator = GucValidator.forVersion("12");
 *   PgParameter param = validator.parameter("wal_level", "replicat");
 *   System.out.printf("Parameter is valid: %s%n", param.isValid());
 *   param.getError().ifPresent(v -> System.out.printf("Error message: %s", v));
 * }</pre>
 *
 * <p>GucValidator instances are thread-safe and PgParameter is immutable.</p>
 *
 * @author Jorge Solórzano
 */
public final class GucValidator {

  private final PgVersion version;
  private final Map<String, PgSetting> parameters;

  /**
   * Construct a GucValidator instance for the version specified.
   *
   * @param postgresVersion the version of PostgreSQL used for validation.
   */
  private GucValidator(PgVersion postgresVersion) {
    this.version = postgresVersion;
    this.parameters = FlatFileReader.forVersion(postgresVersion).toMap();
  }

  /**
   * Construct a new instance used for validation of the parameters based on the version of
   * PostgreSQL.
   *
   * @param postgresVersion used for the validation of parameters.
   * @return instance for validation.
   * @throws NullPointerException if postgresVersion is null.
   * @throws IllegalArgumentException if postgresVersion is invalid or not supported.
   */
  public static GucValidator forVersion(String postgresVersion) {
    Objects.requireNonNull(postgresVersion, "PostgreSQL version is required");
    PgVersion ver = PgVersion.fromString(postgresVersion);
    if (ver == null) {
      throw new IllegalArgumentException(
          String.format(Locale.ROOT,
              "PostgreSQL version \"%s\" is invalid, supported versions are: %s",
              postgresVersion, Arrays.toString(PgVersion.values())));
    }
    return new GucValidator(ver);
  }

  /**
   * Validate a GUC parameter setting.
   *
   * @param name of the parameter.
   * @param setting value of the parameter.
   * @return {@link PgParameter} object with validation data.
   * @throws NullPointerException if the name or setting is null.
   */
  public PgParameter parameter(String name, String setting) {
    Objects.requireNonNull(name, "parameter name is required");
    Objects.requireNonNull(setting, "setting value is required");

    PgSetting pgsetting = parameters.get(name.toLowerCase(Locale.ROOT));
    if (pgsetting != null) {
      return new ParameterValidator(version, pgsetting, setting).validateParameter();
    } else if (!isCustomOption(name)) {
      return ImmutablePgParameter.builder()
          .name(name)
          .setting(setting)
          .error(String.format(Locale.ROOT, "unrecognized configuration parameter \"%s\"", name))
          .build();
    }
    return ImmutablePgParameter.builder()
        .name(name)
        .setting(setting)
        .build();
  }

  private boolean isCustomOption(String name) {
    if (name.indexOf('.') == -1 || name.charAt(0) == '.') {
      // fail-fast if there is no dot or starts with dot.
      return false;
    }
    for (int i = 0; i < name.length(); i++) {
      char ch = name.charAt(i);
      if (!Character.isJavaIdentifierPart(ch) && ch != '.') {
        return false;
      }
    }
    return true;
  }

}
