/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.immutables.value.Value;

@Value.Immutable
@DefaultStyle
abstract class PgSetting {

  abstract String getName();

  abstract VarType getType();

  abstract Optional<String> getDefaultValue();

  abstract Optional<Unit> getDefaultUnit();

  abstract Optional<BigDecimal> getMinValue();

  abstract Optional<BigDecimal> getMaxValue();

  abstract List<String> getEnumValues();

  /**
   * Catch some combinations of types and values.
   */
  @Value.Check
  void check() {
    validateForUnit();
    validateForEnum();
    validateForMinMax();
  }

  private void validateForMinMax() {
    if (getType() == VarType.INTEGER || getType() == VarType.REAL) {
      BigDecimal min = getMinValue().orElse(BigDecimal.ZERO);
      BigDecimal max = getMaxValue().orElse(BigDecimal.ZERO);
      if (getMinValue().isPresent() && getMaxValue().isPresent()) {
        if (min.compareTo(max) > 0) {
          throw new IllegalArgumentException("Maximum value must be greater than minimum value");
        }
      } else {
        throw new IllegalArgumentException("Minimum and maximum value must be present");
      }
    } else if (getMinValue().isPresent() || getMaxValue().isPresent()) {
      throw new IllegalArgumentException("Type must be integer or floating point");
    }
  }

  private void validateForEnum() {
    if (getType() == VarType.ENUM && getEnumValues().isEmpty()) {
      throw new IllegalArgumentException("Enumerated-type parameter must have values");
    }
  }

  private void validateForUnit() {
    getDefaultUnit().ifPresent(unit -> {
      if (getType() != VarType.INTEGER && getType() != VarType.REAL) {
        throw new IllegalArgumentException(String.format(Locale.ROOT,
            "Type must be integer or floating point for unit \"%s\"", unit));
      }
    });
  }

}
