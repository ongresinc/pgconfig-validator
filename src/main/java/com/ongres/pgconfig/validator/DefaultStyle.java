/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.immutables.value.Value;

@Target({ElementType.PACKAGE, ElementType.TYPE})
@Retention(RetentionPolicy.CLASS) // Make it class retention for incremental compilation
@Value.Style(
    visibility = Value.Style.ImplementationVisibility.PACKAGE, // make package-private
    builderVisibility = Value.Style.BuilderVisibility.PACKAGE, // make package-private
    strictBuilder = true, // generate strict builder code
    depluralize = true, // depluralize names for collection and map attributes
    privateNoargConstructor = true, // no argument constructor
    jdkOnly = true, // use only JDK 7+ standard library classes
    defaults = @Value.Immutable(copy = false) // disable copy methods by default
)
@interface DefaultStyle {
}
