/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.math.BigDecimal;

import org.immutables.value.Value;

/**
 * Represents a Numeric with Unit value.
 *
 * <p>Holds the value in the default unit of the setting and a compact format with the largest unit.
 */
@Value.Immutable
@DefaultStyle
interface NumericUnit {

  /**
   * Canonical representation of the numeric value in the default unit.
   *
   * @return value converted to default unit
   */
  BigDecimal getValue();

  /**
   * Default unit that represents the value.
   *
   * @return default unit of setting
   */
  Unit getUnit();

  /**
   * Return the largest divisible unit format.
   *
   * <p>For a setting of '1048576 kB' a value of '1GB' is returned.
   *
   * @return Compact number format with the unit.
   */
  String getSetting();

}
