/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.util.Objects;
import java.util.Optional;

abstract class ValidHandler {

  protected final String value;

  protected final PgSetting setting;

  ValidHandler(String value, PgSetting setting) {
    this.value = Objects.requireNonNull(value, "value must not be null").trim();
    this.setting = Objects.requireNonNull(setting, "setting must not be null");
  }

  abstract boolean isValid();

  abstract Optional<String> getErrorMessage();

}
