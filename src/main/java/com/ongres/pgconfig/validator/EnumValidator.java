/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

final class EnumValidator extends ValidHandler {

  private final List<String> enumvals;

  EnumValidator(String value, PgSetting setting) {
    super(value, setting);
    if (setting.getType() != VarType.ENUM) {
      throw new IllegalArgumentException("Parameter must be of type " + VarType.ENUM);
    }
    if (setting.getEnumValues().isEmpty()) {
      throw new IllegalArgumentException("Enum values cannot be empty");
    }
    this.enumvals = setting.getEnumValues();
  }

  @Override
  Optional<String> getErrorMessage() {
    return isValid()
        ? Optional.empty()
        : Optional.of(String.format(Locale.ROOT, "invalid value for parameter \"%s\": \"%s\"",
            setting.getName(), value));
  }

  /**
   * Enum parameter values are case-insensitive.
   */
  @Override
  boolean isValid() {
    return enumvals.stream()
        .anyMatch(find -> find.equalsIgnoreCase(value));
  }

  String getSetting() {
    return enumvals.stream()
        .filter(find -> find.equalsIgnoreCase(value))
        .findAny().orElse(value);
  }

}
