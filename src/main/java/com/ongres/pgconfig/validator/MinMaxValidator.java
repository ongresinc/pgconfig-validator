/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Optional;

final class MinMaxValidator extends ValidHandler {

  private final BigDecimal numeric;
  private final BigDecimal minVal;
  private final BigDecimal maxVal;

  MinMaxValidator(BigDecimal value, PgSetting setting) {
    super(value.toString(), setting);
    if (setting.getType() != VarType.INTEGER && setting.getType() != VarType.REAL) {
      throw new IllegalArgumentException(
          String.format(Locale.ROOT, "Parameter must be of type %s or %s",
              VarType.INTEGER, VarType.REAL));
    }
    this.numeric = value;
    this.minVal = setting.getMinValue().orElseThrow(IllegalStateException::new);
    this.maxVal = setting.getMaxValue().orElseThrow(IllegalStateException::new);
  }

  @Override
  boolean isValid() {
    return numeric.compareTo(minVal) >= 0
        && numeric.compareTo(maxVal) <= 0;
  }

  @Override
  Optional<String> getErrorMessage() {
    return isValid()
        ? Optional.empty()
        : Optional.of(String.format(Locale.ROOT,
            "%s%s is outside the valid range for parameter \"%s\" (%s .. %s)",
            value, setting.getDefaultUnit().map(m -> " " + m).orElse(""),
            setting.getName(), minVal, maxVal));
  }

}
