/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

import org.immutables.builder.Builder;
import org.immutables.value.Value;

@Value.Style(newBuilder = "builder",
    visibility = Value.Style.ImplementationVisibility.PACKAGE, // make package-private
    builderVisibility = Value.Style.BuilderVisibility.PACKAGE, // make package-private
    jdkOnly = true // use only JDK 7+ standard library classes
)
final class UnitParser {

  private static final BigDecimal MINUS_ONE = BigDecimal.valueOf(-1);

  private static final BigDecimal MULTIPLY_8KB = BigDecimal.valueOf(8);
  private static final BigDecimal MULTIPLY_16MB = BigDecimal.valueOf(16);
  private static final BigDecimal DIVISOR_HOURS = BigDecimal.valueOf(24);
  private static final BigDecimal DIVISOR_MIN_S = BigDecimal.valueOf(60);
  private static final BigDecimal DIVISOR_MS_US = BigDecimal.valueOf(1000);
  private static final BigDecimal DIVISOR_MEMORY = BigDecimal.valueOf(1024);
  private static final BigDecimal DIVISOR_US = new BigDecimal("0.001");

  private UnitParser() {
    // no instances
  }

  private static BigDecimal unitConverter(BigDecimal value, Unit from, Unit to) {
    return from.convert(value, to);
  }

  @Builder.Factory
  static NumericUnit unitParser(String value, Unit defaultUnit, PgVersion version, VarType type) {
    try {
      // Try value without unit first (default unit).
      BigDecimal val = new BigDecimal(value.trim());
      if (type == VarType.INTEGER) {
        val = val.setScale(0, RoundingMode.HALF_DOWN);
      }
      return ImmutableNumericUnit.builder()
          .value(val)
          .unit(defaultUnit)
          .setting(getReadableSetting(val, defaultUnit, version))
          .build();
    } catch (NumberFormatException ignored) {
      // Ignored, assume value with unit
    }

    String[] parsed = parseUnit(value, version);
    if (parsed[0] != null) {
      try {
        BigDecimal parsedVal = new BigDecimal(parsed[0]);
        Unit fromUnit = Unit.fromString(parsed[1]);
        if (fromUnit != null) {
          BigDecimal converted = unitConverter(parsedVal, fromUnit, defaultUnit);
          converted = type == VarType.INTEGER
              ? converted.setScale(0, RoundingMode.HALF_DOWN)
              : converted;
          return ImmutableNumericUnit.builder()
              .value(converted)
              .unit(defaultUnit)
              .setting(getReadableSetting(converted, defaultUnit, version))
              .build();
        }
      } catch (NumberFormatException ignored) {
        // Ignored, invalid number format, Fall Through
      }
    }
    String joinValues = Arrays.stream(Unit.validUnits(defaultUnit.getSubType(), version))
        .map(v -> "\"" + v + "\"")
        .collect(Collectors.joining(", "));
    throw new IllegalArgumentException(
        String.format(Locale.ROOT, "Valid units for this parameter are %s.", joinValues));
  }

  private static String[] parseUnit(String value, PgVersion version) {
    String[] validMemory = Unit.validUnits(VarSubType.MEMORY, version);
    String[] validTime = Unit.validUnits(VarSubType.TIME, version);
    String[] result = Arrays.copyOf(validMemory, validMemory.length + validTime.length);
    System.arraycopy(validTime, 0, result, validMemory.length, validTime.length);

    String[] parsed = new String[2];
    for (String unit : result) {
      int index = value.indexOf(unit);
      if (index != -1 && index != 0 && !Character.isLetter(value.charAt(index - 1))) {
        parsed[0] = value.substring(0, index).trim();
        parsed[1] = value.substring(index).trim();
        break;
      }
    }
    return parsed;
  }

  private static String getReadableSetting(BigDecimal value, Unit unit, PgVersion version) {
    Objects.requireNonNull(value, "value is required");
    Objects.requireNonNull(unit, "unit is required");
    Objects.requireNonNull(version, "version is required");

    // Minus-one values has special meaning, so returned as-is.
    if (MINUS_ONE.compareTo(value) == 0) {
      return MINUS_ONE.toString();
    }

    return unit.getSubType() == VarSubType.MEMORY
        ? convertMemory(value, unit, version)
        : convertTime(value, unit);
  }

  private static String convertMemory(BigDecimal value, Unit unit, PgVersion version) {
    // Normalize block units
    if (unit == Unit.KB_8) {
      value = value.multiply(MULTIPLY_8KB);
      unit = Unit.KB;
    } else if (unit == Unit.MB_16) {
      value = value.multiply(MULTIPLY_16MB);
      unit = Unit.MB;
    }

    if (unit == Unit.B && isDivisible(value, DIVISOR_MEMORY)) {
      value = value.divide(DIVISOR_MEMORY, MathContext.DECIMAL64);
      unit = Unit.KB;
    }
    if (unit == Unit.KB && isDivisible(value, DIVISOR_MEMORY)) {
      value = value.divide(DIVISOR_MEMORY, MathContext.DECIMAL64);
      unit = Unit.MB;
    }
    if (unit == Unit.MB && isDivisible(value, DIVISOR_MEMORY)) {
      value = value.divide(DIVISOR_MEMORY, MathContext.DECIMAL64);
      unit = Unit.GB;
    }
    if (unit == Unit.GB && version.compareTo(Unit.TB.getSinceVersion()) >= 0
        && isDivisible(value, DIVISOR_MEMORY)) {
      value = value.divide(DIVISOR_MEMORY, MathContext.DECIMAL64);
      unit = Unit.TB;
    }
    return getFormatted(value, unit);
  }

  private static String convertTime(BigDecimal value, Unit unit) {
    if (unit == Unit.US && isDivisible(value, DIVISOR_MS_US)) {
      value = value.divide(DIVISOR_MS_US, MathContext.DECIMAL64);
      unit = Unit.MS;
    }
    if (unit == Unit.MS && isDivisible(value, DIVISOR_MS_US)) {
      value = value.divide(DIVISOR_MS_US, MathContext.DECIMAL64);
      unit = Unit.S;
    }
    if (unit == Unit.S && isDivisible(value, DIVISOR_MIN_S)) {
      value = value.divide(DIVISOR_MIN_S, MathContext.DECIMAL64);
      unit = Unit.MIN;
    }
    if (unit == Unit.MIN && isDivisible(value, DIVISOR_MIN_S)) {
      value = value.divide(DIVISOR_MIN_S, MathContext.DECIMAL64);
      unit = Unit.H;
    }
    if (unit == Unit.H && isDivisible(value, DIVISOR_HOURS)) {
      value = value.divide(DIVISOR_HOURS, MathContext.DECIMAL64);
      unit = Unit.D;
    }
    if (unit == Unit.MS && value.scale() > 0) {
      value = value.divide(DIVISOR_US, MathContext.DECIMAL64);
      unit = Unit.US;
    }
    return getFormatted(value, unit);
  }

  private static boolean isDivisible(BigDecimal value, BigDecimal divisor) {
    BigDecimal[] val = value.divideAndRemainder(divisor, MathContext.DECIMAL64);
    return val[0].compareTo(BigDecimal.ONE) >= 0
        && val[1].signum() == 0;
  }

  private static String getFormatted(BigDecimal value, Unit unit) {
    String normalized = value.scale() > 0 ? value.toString() : value.toPlainString();
    return normalized.toLowerCase(Locale.ROOT) + unit;
  }

}
