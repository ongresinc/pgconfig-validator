/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.util.Locale;
import java.util.Optional;

import org.jspecify.annotations.Nullable;

final class BooleanValidator extends ValidHandler {

  private static final String ON = "on";
  private static final String OFF = "off";

  BooleanValidator(String value, PgSetting setting) {
    super(value, setting);
    if (setting.getType() != VarType.BOOL) {
      throw new IllegalArgumentException("Parameter must be of type " + VarType.BOOL);
    }
  }

  @Override
  boolean isValid() {
    return parse() != null;
  }

  @Override
  Optional<String> getErrorMessage() {
    return isValid()
        ? Optional.empty()
        : Optional.of(String.format(Locale.ROOT,
            "parameter \"%s\" requires a Boolean value but was: \"%s\"", setting.getName(), value));
  }

  @Nullable
  String parse() {
    // Case-insensitive and leading or trailing whitespace is ignored.
    String bool = null;
    String val = value.toLowerCase(Locale.ROOT);
    if (ON.equals(val)
        || "true".equals(val)
        || "t".equals(val)
        || "yes".equals(val)
        || "y".equals(val)
        || "1".equals(val)) {
      bool = ON;
    } else if (OFF.equals(val)
        || "false".equals(val)
        || "f".equals(val)
        || "no".equals(val)
        || "n".equals(val)
        || "0".equals(val)) {
      bool = OFF;
    }
    return bool;
  }

}
