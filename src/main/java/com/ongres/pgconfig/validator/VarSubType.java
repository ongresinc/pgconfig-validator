/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.util.Locale;

enum VarSubType {

  MEMORY,
  TIME;

  @Override
  public String toString() {
    return name().toLowerCase(Locale.ROOT);
  }

}
