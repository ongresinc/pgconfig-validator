/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * The validator package provides validation for PostgreSQL GUC parameters settings.
 *
 * <p>PostgreSQL Configuration Parameter Validator gives you the ability to verify if a GUC (Grand
 * Unified Configuration) parameter is valid based on a few rules that are checked depending on the
 * PostgreSQL server version being used.
 *
 * <p>All parameter names are case-insensitive. Every parameter takes a value of one of five types:
 * boolean, string, integer, floating point, or enumerated (enum). The type determines the syntax
 * for setting the parameter:
 *
 * <ul><li><p><b>Boolean:</b> Values can be written as on, off, true, false, yes, no, 1, 0 (all
 * case-insensitive) or any unambiguous prefix of one of these.</li>
 *
 * <li><p><b>String:</b> In general, a text type that can hold any value, this means that no further
 * validation is done.</li>
 *
 * <li><p><b>Numeric (integer and floating point):</b> A decimal point is permitted only for
 * floating-point parameters. Do not use thousands separators.</li>
 *
 * <li><p><b>Numeric with Unit:</b> Some numeric parameters have an implicit unit, because they
 * describe quantities of memory or time. The unit might be kilobytes, blocks (typically eight
 * kilobytes), milliseconds, seconds, or minutes. An unadorned numeric value for one of these
 * settings will use the setting's default unit. For convenience, settings can be given with a unit
 * specified explicitly, for example {@code '120 ms'} for a time value, and they will be converted
 * to whatever the parameter's actual unit is. The unit name is case-sensitive, and there can be
 * whitespace between the numeric value and the unit.
 *
 * <ul><li> <p>Valid <i>memory</i> units are <b>kB</b> (kilobytes), <b>MB</b> (megabytes), and
 * <b>GB</b> (gigabytes). Depending on the PostgreSQL version it could also allow <b>B</b> (bytes),
 * and <b>TB</b> (terabytes).</li>
 *
 * <li><p>Valid <i>time</i> units are <b>ms</b> (milliseconds), <b>s</b> (seconds), <b>min</b>
 * (minutes), <b>h</b> (hours), and <b>d</b> (days). Depending on the PostgreSQL version it could
 * also allow <b>us</b> (microseconds).</li></ul>
 *
 * <p>If a fractional value is specified with a unit, it will be rounded to a multiple of the next
 * smaller unit if there is one. For example, 30.1 GB will be converted to 30822 MB not 32319628902
 * B. If the parameter is of integer type, a final rounding to integer occurs after any units
 * conversion.</li>
 *
 * <li><p><b>Enumerated:</b> Enumerated-type parameters are written in the same way as string
 * parameters, but are restricted to have one of a limited set of values. Enum parameter values are
 * case-insensitive.</li></ul>
 */

@org.jspecify.annotations.NullMarked
package com.ongres.pgconfig.validator;
