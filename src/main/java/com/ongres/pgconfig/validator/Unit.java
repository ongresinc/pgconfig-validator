/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import static com.ongres.pgconfig.validator.VarSubType.MEMORY;
import static com.ongres.pgconfig.validator.VarSubType.TIME;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.jspecify.annotations.Nullable;

enum Unit {

  /**
   * Byte.
   */
  B("B", MEMORY, PgVersion.V11, BigDecimal.ONE, "B"),
  /**
   * Kilobytes.
   */
  KB("kB", MEMORY, PgVersion.V9_1, BigDecimal.valueOf(1024), "B"),
  /**
   * Eight kilobytes (blocks).
   */
  KB_8("8kB", MEMORY, PgVersion.V9_1, BigDecimal.valueOf(8192), "kB"),
  /**
   * Megabytes.
   */
  MB("MB", MEMORY, PgVersion.V9_1, BigDecimal.valueOf(1048576), "kB"),
  /**
   * Sixteen megabytes.
   */
  MB_16("16MB", MEMORY, PgVersion.V9_1, BigDecimal.valueOf(16777216), "MB"),
  /**
   * Gigabytes.
   */
  GB("GB", MEMORY, PgVersion.V9_1, BigDecimal.valueOf(1073741824), "MB"),
  /**
   * Terabytes.
   */
  TB("TB", MEMORY, PgVersion.V9_4, BigDecimal.valueOf(1099511627776L), "GB"),
  /**
   * Microseconds.
   */
  US("us", TIME, PgVersion.V12, BigDecimal.ONE, "us"),
  /**
   * Milliseconds.
   */
  MS("ms", TIME, PgVersion.V9_1, BigDecimal.valueOf(1000), "us"),
  /**
   * Seconds.
   */
  S("s", TIME, PgVersion.V9_1, BigDecimal.valueOf(1000000), "ms"),
  /**
   * Minutes.
   */
  MIN("min", TIME, PgVersion.V9_1, BigDecimal.valueOf(60000000), "s"),
  /**
   * Hours.
   */
  H("h", TIME, PgVersion.V9_1, BigDecimal.valueOf(3600000000L), "min"),
  /**
   * Days.
   */
  D("d", TIME, PgVersion.V9_1, BigDecimal.valueOf(86400000000L), "h");

  private static final Map<String, Unit> VALUES_BY_NAME = Arrays.stream(values())
      .collect(Collectors.toMap(Unit::toString, Function.identity()));

  private final String name;
  private final VarSubType subType;

  private final PgVersion since;

  private final BigDecimal factor;

  private final String to;

  Unit(String name, VarSubType subType, PgVersion since, BigDecimal factor, String to) {
    this.name = name;
    this.subType = subType;
    this.since = since;
    this.factor = factor;
    this.to = to;
  }

  VarSubType getSubType() {
    return subType;
  }

  PgVersion getSinceVersion() {
    return since;
  }

  BigDecimal convert(BigDecimal source, Unit destinationUnit, PgVersion version) {
    if (version.compareTo(this.since) < 0) {
      throw new IllegalArgumentException(String.format(Locale.ROOT,
          "Source unit \"%s\" is not compatible with version \"%s\"",
          this, version));
    } else if (version.compareTo(destinationUnit.since) < 0) {
      throw new IllegalArgumentException(String.format(Locale.ROOT,
          "Destination unit \"%s\" is not compatible with version \"%s\"",
          destinationUnit, version));
    }

    return convert(source, destinationUnit);
  }

  BigDecimal convert(BigDecimal source, Unit destinationUnit) {
    if (this == destinationUnit) {
      return source;
    }
    if (this.subType != destinationUnit.subType) {
      throw new IllegalArgumentException(String.format(Locale.ROOT,
          "Source unit \"%s\" is not compatible with destination unit \"%s\"",
          this, destinationUnit));
    }

    if (source.scale() == 0) {
      return calcFactor(source, this.factor, destinationUnit.factor);
    }

    Unit toUnit = fromStringOrDefault(to, this);
    if (toUnit != this) {
      return toUnit.convert(calcFactor(source, this.factor, toUnit.factor)
          .setScale(0, RoundingMode.HALF_DOWN), destinationUnit);
    }

    return calcFactor(source, this.factor, destinationUnit.factor)
        .setScale(6, RoundingMode.HALF_DOWN);
  }

  private BigDecimal calcFactor(BigDecimal value, BigDecimal fromFactor, BigDecimal toFactor) {
    return value.multiply(fromFactor, MathContext.DECIMAL128)
        .divide(toFactor, MathContext.DECIMAL128);
  }

  @Override
  public String toString() {
    return name;
  }

  @Nullable
  static Unit fromString(String name) {
    return VALUES_BY_NAME.get(name);
  }

  private static Unit fromStringOrDefault(String name, Unit defaultValue) {
    return VALUES_BY_NAME.getOrDefault(name, defaultValue);
  }

  static String[] validUnits(VarSubType subType, PgVersion version) {
    return Arrays.stream(values())
        .filter(st -> st.subType == subType)
        .filter(v -> version.compareTo(v.since) >= 0)
        .filter(blocks -> blocks != KB_8 && blocks != MB_16)
        .map(Unit::toString)
        .toArray(String[]::new);
  }

}
