/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.util.Optional;

import org.immutables.value.Value;

/**
 * Immutable representation of {@code PgParameter} to check if the setting of a parameter is valid.
 *
 * @author Jorge Solórzano
 */
@Value.Immutable
@DefaultStyle
public interface PgParameter {

  /**
   * Parameter name.
   *
   * @return the same name as the lookup parameter
   */
  String getName();

  /**
   * The setting parsed and validated, or if there is an error, the same input value.
   *
   * <p>For a numeric with unit setting of '2048 kB' a value of '2 MB' is returned.
   *
   * @return setting value
   */
  String getSetting();

  /**
   * If there is an error on the parsed setting value, a description error mesage is returned.
   *
   * @return String with the error message if the parameter is invalid.
   */
  Optional<String> getError();

  /**
   * If there is an error on the parsed setting value, an optional suggestion on how to fix the
   * error is returned.
   *
   * @return String with the hint message on how to fix the error (if exists).
   */
  Optional<String> getHint();

  /**
   * Check if the parameter setting is valid.
   *
   * @return true if there are no errors in the setting value.
   */
  default boolean isValid() {
    return getError().isEmpty();
  }

}
