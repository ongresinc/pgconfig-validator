/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Flat file parser to transform a CSV (Text COPY format) into a Map.
 */
final class FlatFileReader {

  private final String fileName;

  private FlatFileReader(PgVersion version) {
    this.fileName = "guc-info/params-" + version.name() + ".csv";
  }

  static FlatFileReader forVersion(PgVersion version) {
    Objects.requireNonNull(version, "PostgreSQL version is missing or invalid");
    return new FlatFileReader(version);
  }

  /**
   * Transform a list of parameters TSV file (COPY format) into a java.util.Map of PgParameter with
   * the parameter name as key object. The key name is converted to lower case since all parameter
   * names are case-insensitive, when doing the lookup convert the name to lower case to get a
   * match.
   *
   * @return Map of PgSetting with lower-case parameter name as key.
   */
  Map<String, PgSetting> toMap() {
    try (BufferedReader br = new BufferedReader(
        new InputStreamReader(getResource(fileName), StandardCharsets.UTF_8));
        Stream<String> input = br.lines()) {
      return input.map(FlatFileReader::mapToParameter)
          .collect(Collectors.toMap(key -> key.getName().toLowerCase(Locale.ROOT),
              Function.identity()));
    } catch (IOException cause) {
      // Throws on close of BufferedReader, but should never happen.
      throw new UncheckedIOException(cause.getMessage(), cause);
    }
  }

  /**
   * Mapper function of a single line parameter to a PgSetting object.
   */
  private static PgSetting mapToParameter(String line) {
    String[] field = line.split("\t", -1);
    return ImmutablePgSetting.builder()
        .name(field[0])
        .type(VarType.fromString(field[1]))
        .defaultValue(fromNull(field[2]))
        .defaultUnit(Optional.ofNullable(Unit.fromString(fromNull(field[3]).orElse(""))))
        .minValue(toBigDecimal(field[4]))
        .maxValue(toBigDecimal(field[5]))
        .addEnumValues(toEnum(field[6]))
        .build();
  }

  /**
   * The specified null string is sent by COPY TO without adding any backslashes.
   */
  private static Optional<String> fromNull(String value) {
    return "\\N".equals(value) ? Optional.empty() : Optional.of(value);
  }

  /**
   * Convert String to BigDecimal, return null if null string (\N) is returned.
   */
  private static Optional<BigDecimal> toBigDecimal(String value) {
    return fromNull(value).isPresent() ? Optional.of(new BigDecimal(value)) : Optional.empty();
  }

  /**
   * Transform an enum string like {terse,default,verbose} to a String[].
   */
  private static String[] toEnum(String value) {
    return fromNull(value).isPresent()
        ? value.substring(1, value.length() - 1)
            .replace("\"", "").split(",", -1)
        : new String[0];
  }

  /**
   * Just a helper method to read resources from classpath.
   *
   * @param resourceName to lookup in the class loader.
   * @return URI with the path of the resource.
   * @throws IllegalStateException if the resources is not found.
   */
  private InputStream getResource(String resourceName) {
    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    return loader.getResourceAsStream(resourceName);
  }

}
