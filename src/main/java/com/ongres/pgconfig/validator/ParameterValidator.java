/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Supplier;

import org.jspecify.annotations.Nullable;

final class ParameterValidator {

  private static final List<String> paramOctalFormat =
      Arrays.asList("unix_socket_permissions", "log_file_mode");

  private final PgVersion version;
  private final PgSetting pgsetting;
  private final String setting;
  private final Supplier<String> invalidValueMsg;

  public ParameterValidator(PgVersion version, PgSetting pgsetting, String setting) {
    super();
    this.version = version;
    this.pgsetting = pgsetting;
    this.setting = setting;
    this.invalidValueMsg =
        () -> String.format(Locale.ROOT, "invalid value for parameter \"%s\": \"%s\"",
            pgsetting.getName(), setting);
  }

  PgParameter validateParameter() {
    switch (pgsetting.getType()) {
      case BOOL:
        return validateBoolean();
      case INTEGER:
      case REAL:
        return validateNumeric();
      case ENUM:
        return validateEnumerated();
      case STRING:
      default:
        // Strings are not validated so assume to be valid.
        return ImmutablePgParameter.builder()
            .name(pgsetting.getName())
            .setting(setting)
            .build();
    }
  }

  private PgParameter validateNumeric() {
    BigDecimal settingNumber = null;
    String newSetting = null;

    if (pgsetting.getType() == VarType.INTEGER) {
      // Hexadecimal and Octal handling
      settingNumber = parseHexOctal(setting.trim());
    }

    Optional<Unit> defaultUnit = pgsetting.getDefaultUnit();
    if (defaultUnit.isPresent()) {
      try {
        NumericUnit integerUnit = UnitParserBuilder.builder()
            .value(settingNumber == null ? setting : settingNumber.toPlainString())
            .defaultUnit(defaultUnit.orElseThrow())
            .version(version)
            .type(pgsetting.getType())
            .build();
        settingNumber = integerUnit.getValue();
        newSetting = integerUnit.getSetting();
      } catch (IllegalArgumentException e) {
        return ImmutablePgParameter.builder()
            .name(pgsetting.getName())
            .setting(setting)
            .error(invalidValueMsg.get())
            .hint(Optional.ofNullable(e.getMessage()))
            .build();
      }
    }

    if (settingNumber == null) {
      try {
        settingNumber = new BigDecimal(setting.trim());
      } catch (NumberFormatException e) {
        return ImmutablePgParameter.builder()
            .name(pgsetting.getName())
            .setting(setting)
            .error(invalidValueMsg.get())
            .hint("Value must be of type " + pgsetting.getType())
            .build();
      }
    }

    MinMaxValidator minMaxInteger = new MinMaxValidator(settingNumber, pgsetting);
    if (!minMaxInteger.isValid()) {
      return ImmutablePgParameter.builder()
          .name(pgsetting.getName())
          .setting(setting)
          .error(minMaxInteger.getErrorMessage().orElseGet(invalidValueMsg))
          .build();
    }

    if (newSetting == null) {
      newSetting = showFormat(pgsetting, settingNumber);
    }

    return ImmutablePgParameter.builder()
        .name(pgsetting.getName())
        .setting(newSetting)
        .build();
  }

  private PgParameter validateEnumerated() {
    EnumValidator enumerated = new EnumValidator(setting, pgsetting);
    if (enumerated.isValid()) {
      return ImmutablePgParameter.builder()
          .name(pgsetting.getName())
          .setting(enumerated.getSetting())
          .build();
    } else {
      String hintMessage = String.format(Locale.ROOT, "Available values: %s",
          String.join(", ", pgsetting.getEnumValues()));
      return ImmutablePgParameter.builder()
          .name(pgsetting.getName())
          .setting(setting)
          .error(enumerated.getErrorMessage().orElseGet(invalidValueMsg))
          .hint(hintMessage)
          .build();
    }
  }

  private PgParameter validateBoolean() {
    BooleanValidator bool = new BooleanValidator(setting, pgsetting);
    String newBoolean = bool.parse();
    if (newBoolean != null) {
      return ImmutablePgParameter.builder()
          .name(pgsetting.getName())
          .setting(newBoolean)
          .build();
    } else {
      return ImmutablePgParameter.builder()
          .name(pgsetting.getName())
          .setting(setting)
          .error(bool.getErrorMessage().orElseGet(invalidValueMsg))
          .hint("Allowed values: on, off")
          .build();
    }
  }

  private String showFormat(PgSetting pgsetting, BigDecimal settingNumber) {
    if (pgsetting.getType() == VarType.INTEGER) {
      BigDecimal scaled = settingNumber.setScale(0, RoundingMode.HALF_DOWN);
      if (paramOctalFormat.contains(pgsetting.getName())) {
        return String.format(Locale.ROOT, "%04o", scaled.intValue());
      }
      // Integer type don't have large values, return plain string.
      return scaled.toPlainString();
    } else {
      // Return scientific notation if an exponent is needed,
      // lowercase the 'e' to match PostgreSQL representation.
      return settingNumber.toString().toLowerCase(Locale.ROOT);
    }
  }

  private @Nullable BigDecimal parseHexOctal(String value) {
    try {
      return BigDecimal.valueOf(Integer.decode(value));
    } catch (NumberFormatException ignored) {
      // ignored, not a valid Hexadecimal or Octal notation.
      return null;
    }
  }

}
