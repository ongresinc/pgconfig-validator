/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.jspecify.annotations.Nullable;

/**
 * List of supported versions of PostgreSQL.
 */
enum PgVersion {

  V9_1("9.1"),
  V9_2("9.2"),
  V9_3("9.3"),
  V9_4("9.4"),
  V9_5("9.5"),
  V9_6("9.6"),
  V10("10"),
  V11("11"),
  V12("12"),
  V13("13"),
  V14("14"),
  V15("15"),
  V16("16"),
  V17("17");

  private static final Map<String, PgVersion> VALUES_BY_NAME = Arrays.stream(values())
      .collect(Collectors.toMap(PgVersion::toString, Function.identity()));

  private final String version;

  PgVersion(String version) {
    this.version = version;
  }

  @Override
  public String toString() {
    return version;
  }

  @Nullable
  public static PgVersion fromString(String version) {
    return VALUES_BY_NAME.get(version);
  }

}
