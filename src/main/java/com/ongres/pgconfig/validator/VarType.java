/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import java.util.Locale;

enum VarType {

  STRING,
  ENUM,
  INTEGER,
  REAL,
  BOOL;

  @Override
  public String toString() {
    return name().toLowerCase(Locale.ROOT);
  }

  static VarType fromString(String name) {
    switch (name) {
      case "string":
        return STRING;
      case "enum":
        return ENUM;
      case "integer":
        return INTEGER;
      case "real":
        return REAL;
      case "bool":
        return BOOL;
      default:
        throw new IllegalArgumentException(name + " type is invalid.");
    }
  }

}
