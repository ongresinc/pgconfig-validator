/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package test.publicapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Locale;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import com.ongres.pgconfig.validator.GucValidator;
import com.ongres.pgconfig.validator.PgParameter;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class HexOctalNumericTest {

  private static final Random RAND = new Random();

  @ParameterizedTest
  @ValueSource(strings = {"unix_socket_permissions", "log_file_mode"})
  void testIsValidOctal(String paramName) {
    GucValidator validator = GucValidator.forVersion("12");
    PgParameter param = validator.parameter(paramName, "0640");

    assertEquals(paramName, param.getName());
    assertEquals("0640", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"unix_socket_permissions", "log_file_mode"})
  void testConvertedValidOctal(String paramName) {
    GucValidator validator = GucValidator.forVersion("12");
    PgParameter param = validator.parameter(paramName, "511");

    assertEquals(paramName, param.getName());
    assertEquals("0777", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"unix_socket_permissions", "log_file_mode"})
  void testConvertedFactionalOctal(String paramName) {
    GucValidator validator = GucValidator.forVersion("12");
    PgParameter param = validator.parameter(paramName, "410.7");

    assertEquals(paramName, param.getName());
    assertEquals("0633", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"9.1", "9.2", "9.3", "9.4", "9.5", "9.6", "10", "11", "12"})
  void testIsValidOctalInteger(String version) {
    GucValidator validator = GucValidator.forVersion(version);
    PgParameter param = validator.parameter("vacuum_cost_limit", "0377");

    assertEquals("vacuum_cost_limit", param.getName());
    assertEquals("255", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"9.1", "9.2", "9.3", "9.4", "9.5", "9.6", "10", "11", "12"})
  void testIsValidNegativeHexadecimal(String version) {
    GucValidator validator = GucValidator.forVersion(version);
    PgParameter param = validator.parameter("extra_float_digits", "-0xA");

    assertEquals("extra_float_digits", param.getName());
    assertEquals("-10", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"9.1", "9.2", "9.3", "9.4", "9.5", "9.6", "10", "11", "12"})
  void testValidHexadecimal(String version) {
    GucValidator validator = GucValidator.forVersion(version);
    PgParameter param = validator.parameter("max_connections", "0xFFFF");

    assertEquals("max_connections", param.getName());
    assertEquals("65535", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"9.1", "9.2", "9.3", "9.4", "9.5", "9.6", "10", "11", "12"})
  void testInvalidHexadecimal(String version) {
    GucValidator validator = GucValidator.forVersion(version);
    PgParameter param = validator.parameter("max_connections", "0xFFPF");

    assertEquals("max_connections", param.getName());
    assertEquals("0xFFPF", param.getSetting());
    assertEquals("invalid value for parameter \"max_connections\": \"0xFFPF\"",
        param.getError().orElseThrow());
    assertEquals("Value must be of type integer", param.getHint().orElseThrow());
  }

  @ParameterizedTest
  @ValueSource(strings = {"0", "0x0", "0000", "00", "-0x000", "-00000"})
  void test_unix_socket_permissions_zero_values(String value) {
    GucValidator validator = GucValidator.forVersion("12");
    PgParameter param = validator.parameter("unix_socket_permissions", value);

    assertEquals("unix_socket_permissions", param.getName());
    assertEquals("0000", param.getSetting());
    assertTrue(param.isValid());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  private static Stream<Arguments> provideStringsForOctalParameters() {
    return RAND.ints(25, 0, 511)
        .mapToObj(i -> Arguments.of(String.valueOf(i), String.format(Locale.ROOT, "%04o", i)));
  }

  @ParameterizedTest
  @MethodSource("provideStringsForOctalParameters")
  void test_unix_socket_permissions_values(String value, String expected) {
    GucValidator validator = GucValidator.forVersion("12");
    PgParameter param = validator.parameter("unix_socket_permissions", value);

    assertTrue(param.isValid());
    assertEquals("unix_socket_permissions", param.getName());
    assertEquals(expected, param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"0", "0x0", "0000", "-0x000", "-0000"})
  void test_wal_keep_size_zero_values(String value) {
    GucValidator validator = GucValidator.forVersion("13");
    PgParameter param = validator.parameter("wal_keep_size", value);

    assertEquals("wal_keep_size", param.getName());
    assertEquals("0MB", param.getSetting());
    assertTrue(param.isValid());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"wal_keep_size", "unix_socket_permissions",
      "wal_receiver_timeout", "bgwriter_lru_maxpages"})
  void test_empty_values_integer_parameters(String paramName) {
    GucValidator validator = GucValidator.forVersion("13");
    PgParameter param = validator.parameter(paramName, "");

    assertEquals(paramName, param.getName());
    assertEquals("", param.getSetting());
    assertFalse(param.isValid());
    assertEquals("invalid value for parameter \"" + paramName + "\": \"\"",
        param.getError().orElseThrow());
    assertTrue(param.getHint().isPresent());
  }

}
