/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package test.publicapi;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import com.ongres.pgconfig.validator.GucValidator;
import com.ongres.pgconfig.validator.PgParameter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

final class PublicApiTest {

  @ParameterizedTest
  @ValueSource(strings = {"10", "11", "12", "13", "14", "15", "16"})
  void testForVersion(String version) {
    GucValidator validator = GucValidator.forVersion(version);
    PgParameter param = validator.parameter("Application_name", "Validator");
    assertTrue(param.isValid());
    assertEquals(Optional.empty(), param.getError());
    assertEquals("application_name", param.getName());
    assertEquals("Validator", param.getSetting());
  }

  @ParameterizedTest
  @ValueSource(strings = {"7.2", "8.4", "9.0", "demo", "$$", "x.4", "\"", "1", "12.1"})
  void testInvalidPostgresVersion(String version) {
    IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
        () -> GucValidator.forVersion(version));

    assertNotNull(ex.getMessage());
    assertTrue(ex.getMessage().startsWith("PostgreSQL version \"" + version + "\" is invalid"),
        "PostgreSQL version " + version + " is invalid");
  }

  @Test
  void testBooleanParameterInvalid() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("data_sync_retry", "Validator");
    assertFalse(param.isValid());
    assertEquals("parameter \"data_sync_retry\" requires a Boolean value but was: \"Validator\"",
        param.getError().orElseThrow());
    assertEquals("data_sync_retry", param.getName());
    assertEquals("Validator", param.getSetting());
  }

  @ParameterizedTest
  @ValueSource(strings = {"1", "y", "t", "oN", "yEs", "TRUE"})
  void testOnBooleanParameterValid(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("autovacuum", input);
    assertTrue(param.isValid());
    assertEquals(Optional.empty(), param.getError());
    assertEquals("autovacuum", param.getName());
    assertEquals("on", param.getSetting());
  }

  @ParameterizedTest
  @ValueSource(strings = {"0", "N", "f", "ofF", "NO", "False"})
  void testOffBooleanParameterValid(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("autovacuum", input);
    assertTrue(param.isValid());
    assertEquals(Optional.empty(), param.getError());
    assertEquals("autovacuum", param.getName());
    assertEquals("off", param.getSetting());
  }

  @Test
  void testEnumParameterInvalid() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("wal_level", "replication");
    assertFalse(param.isValid());
    assertEquals("invalid value for parameter \"wal_level\": \"replication\"",
        param.getError().orElseThrow());
    assertEquals("wal_level", param.getName());
    assertEquals("replication", param.getSetting());
    assertEquals("Available values: minimal, replica, logical", param.getHint().orElseThrow());
  }

  @ParameterizedTest
  @ValueSource(strings = {"minimal", "replica", "logical"})
  void testEnumParameterValid_wal_level(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("wal_level", input);
    assertTrue(param.isValid());
    assertEquals("wal_level", param.getName());
    assertEquals(input, param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"Debug2", "deBug1", "loG", "noTIce", "WARNing", "erroR"})
  void testEnumParameterValid_client_min_messages(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("client_min_messages", input);
    assertTrue(param.isValid());
    assertEquals("client_min_messages", param.getName());
    assertEquals(input.toLowerCase(Locale.ROOT), param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"1tb", "1tB", "1Tb", "1TiB", "10240KB", "10gb"})
  void testIntegerUnitParameterInvalid(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("work_mem", input);
    assertFalse(param.isValid());
    assertEquals("work_mem", param.getName());
    assertEquals(input, param.getSetting());
    assertEquals("invalid value for parameter \"work_mem\": \"" + input + "\"",
        param.getError().orElseThrow());
    assertEquals("Valid units for this parameter are \"B\", \"kB\", \"MB\", \"GB\", \"TB\".",
        param.getHint().orElseThrow());
  }

  @Test
  void testIntegerParameterInvalid() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("extra_float_digits", "hello");
    assertFalse(param.isValid());
    assertEquals("extra_float_digits", param.getName());
    assertEquals("hello", param.getSetting());
    assertEquals("invalid value for parameter \"extra_float_digits\": \"hello\"",
        param.getError().orElseThrow());
    assertEquals("Value must be of type integer", param.getHint().orElseThrow());
  }

  @Test
  void testPg15ParameterValid() {
    GucValidator validator = GucValidator.forVersion("15");
    PgParameter param = validator.parameter("wal_decode_buffer_size", "8388608");
    assertTrue(param.isValid());
    assertEquals("wal_decode_buffer_size", param.getName());
    assertEquals("8MB", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testIntegerParameterValid() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("extra_float_digits", "-15");
    assertTrue(param.isValid());
    assertEquals("extra_float_digits", param.getName());
    assertEquals("-15", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testIntegerUnitParameterInvalid2() {
    GucValidator validator = GucValidator.forVersion("9.3");
    PgParameter param = validator.parameter("work_mem", "1 TB");
    assertFalse(param.isValid());
    assertEquals("work_mem", param.getName());
    assertEquals("1 TB", param.getSetting());
    assertEquals("invalid value for parameter \"work_mem\": \"1 TB\"",
        param.getError().orElseThrow());
    assertEquals("Valid units for this parameter are \"kB\", \"MB\", \"GB\".",
        param.getHint().orElseThrow());
  }

  @Test
  void testIntegerUnitParameterInvalid3() {
    GucValidator validator = GucValidator.forVersion("10");
    PgParameter param = validator.parameter("work_mem", "4096 TB");
    assertFalse(param.isValid());
    assertEquals("work_mem", param.getName());
    assertEquals("4096 TB", param.getSetting());
    assertEquals(
        "4398046511104 kB is outside the valid range for parameter \"work_mem\" (64 .. 2147483647)",
        param.getError().orElseThrow());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testIntegerUnitParameterInvalid4() {
    GucValidator validator = GucValidator.forVersion("10");
    PgParameter param = validator.parameter("work_mem", "16  kB");
    assertFalse(param.isValid());
    assertEquals("work_mem", param.getName());
    assertEquals("16  kB", param.getSetting());
    assertEquals(
        "16 kB is outside the valid range for parameter \"work_mem\" (64 .. 2147483647)",
        param.getError().orElseThrow());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testIntegerUnitParameterInvalid5() {
    GucValidator validator = GucValidator.forVersion("10");
    PgParameter param = validator.parameter("work_mem", "kB1048576");
    assertFalse(param.isValid());
    assertEquals("work_mem", param.getName());
    assertEquals("kB1048576", param.getSetting());
    assertEquals("invalid value for parameter \"work_mem\": \"kB1048576\"",
        param.getError().orElseThrow());
    assertEquals("Valid units for this parameter are \"kB\", \"MB\", \"GB\", \"TB\".",
        param.getHint().orElseThrow());
  }

  @Test
  void testIntegerUnitParameterInvalid6() {
    GucValidator validator = GucValidator.forVersion("10");
    PgParameter param = validator.parameter("work_mem", "MB10MB48576");
    assertFalse(param.isValid());
    assertEquals("work_mem", param.getName());
    assertEquals("MB10MB48576", param.getSetting());
    assertEquals("invalid value for parameter \"work_mem\": \"MB10MB48576\"",
        param.getError().orElseThrow());
    assertEquals("Valid units for this parameter are \"kB\", \"MB\", \"GB\", \"TB\".",
        param.getHint().orElseThrow());
  }

  @Test
  void testIntegerUnitParameterInvalid7() {
    GucValidator validator = GucValidator.forVersion("10");
    PgParameter param = validator.parameter("vacuum_cost_delay", "10h 48576s");
    assertFalse(param.isValid());
    assertEquals("vacuum_cost_delay", param.getName());
    assertEquals("10h 48576s", param.getSetting());
    assertEquals("invalid value for parameter \"vacuum_cost_delay\": \"10h 48576s\"",
        param.getError().orElseThrow());
    assertEquals("Valid units for this parameter are \"ms\", \"s\", \"min\", \"h\", \"d\".",
        param.getHint().orElseThrow());
  }

  @ParameterizedTest
  @CsvSource({"13,1048576 MB,1TB", "13,1048575 MB,1048575MB",
      "9.1,1048576 MB,1024GB", "9.2,1048577MB,1048577MB"})
  void testIntegerUnitParameterValid(String version, String input, String expected) {
    GucValidator validator = GucValidator.forVersion(version);
    PgParameter param = validator.parameter("work_mem", input);
    assertTrue(param.isValid());
    assertEquals("work_mem", param.getName());
    assertEquals(expected, param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @CsvSource({"0.05  ms,0ms", "500 us,0ms", "0.6,1ms", "10.7,11ms", "0.0059,0ms",
      "123E-4s,12ms", "123E-15,0ms", "0.00000000000000123,0ms", "123E-15min,0ms"})
  void testIntegerUnitParameterValid(String input, String expected) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("idle_in_transaction_session_timeout", input);
    assertTrue(param.isValid());
    assertEquals("idle_in_transaction_session_timeout", param.getName());
    assertEquals(expected, param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @CsvSource({"0.05  ms,50us", "500 us,500us", "0.6,600us", "10.7,10700us", "0.0059,5.9us",
      "123E-4s,12ms", "123E-15,1.23e-10us", "0.00000000000000123,1.23e-12us", "123E-15min,0ms"})
  void testRealUnitParameterValid(String input, String expected) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("vacuum_cost_delay", input);
    assertTrue(param.isValid());
    assertEquals("vacuum_cost_delay", param.getName());
    assertEquals(expected, param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testRealUnitParameterValid2() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("vacuum_cost_delay", "  500  us  ");
    assertTrue(param.isValid());
    assertEquals("vacuum_cost_delay", param.getName());
    assertEquals("500us", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testRealParameterValid() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("cursor_tuple_fraction", "  0.3  ");
    assertTrue(param.isValid());
    assertEquals("cursor_tuple_fraction", param.getName());
    assertEquals("0.3", param.getSetting());
    assertEquals(Optional.empty(), param.getError());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testRealParameterInvalid() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("cursor_tuple_fraction", "hello");
    assertFalse(param.isValid());
    assertEquals("cursor_tuple_fraction", param.getName());
    assertEquals("hello", param.getSetting());
    assertEquals("invalid value for parameter \"cursor_tuple_fraction\": \"hello\"",
        param.getError().orElseThrow());
    assertEquals("Value must be of type real", param.getHint().orElseThrow());
  }

  @Test
  void testRealParameterInvalid3() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("cursor_tuple_fraction", "-1");
    assertFalse(param.isValid());
    assertEquals("cursor_tuple_fraction", param.getName());
    assertEquals("-1", param.getSetting());
    assertEquals("-1 is outside the valid range for parameter \"cursor_tuple_fraction\" (0 .. 1)",
        param.getError().orElseThrow());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testRealParameterInvalid4() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("cursor_tuple_fraction", "1.001");
    assertFalse(param.isValid());
    assertEquals("cursor_tuple_fraction", param.getName());
    assertEquals("1.001", param.getSetting());
    assertEquals(
        "1.001 is outside the valid range for parameter \"cursor_tuple_fraction\" (0 .. 1)",
        param.getError().orElseThrow());
    assertEquals(Optional.empty(), param.getHint());
  }

  @Test
  void testRealParameterInvalid5() {
    GucValidator validator = GucValidator.forVersion("9.1");
    PgParameter param = validator.parameter("vacuum_cost_delay", "100.001us");
    assertFalse(param.isValid());
    assertEquals("vacuum_cost_delay", param.getName());
    assertEquals("100.001us", param.getSetting());
    assertEquals("invalid value for parameter \"vacuum_cost_delay\": \"100.001us\"",
        param.getError().orElseThrow());
    assertEquals("Valid units for this parameter are \"ms\", \"s\", \"min\", \"h\", \"d\".",
        param.getHint().orElseThrow());
  }

  @Test
  void testRealParameterInvalid6() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("vacuum_cost_delay", "100xx");
    assertFalse(param.isValid());
    assertEquals("vacuum_cost_delay", param.getName());
    assertEquals("100xx", param.getSetting());
    assertEquals("invalid value for parameter \"vacuum_cost_delay\": \"100xx\"",
        param.getError().orElseThrow());
    assertEquals("Valid units for this parameter are \"us\", \"ms\", \"s\", \"min\", \"h\", \"d\".",
        param.getHint().orElseThrow());
  }

  @Test
  void testRealParameterInvalid7() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("effective_io_concurrency", "1MB");
    assertFalse(param.isValid());
    assertEquals("effective_io_concurrency", param.getName());
    assertEquals("1MB", param.getSetting());
    assertEquals("invalid value for parameter \"effective_io_concurrency\": \"1MB\"",
        param.getError().orElseThrow());
    assertEquals("Value must be of type integer",
        param.getHint().orElseThrow());
  }

  @Test
  void testRealParameterInvalid8() {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter("effective_io_concurreny", "2");
    assertFalse(param.isValid());
    assertEquals("effective_io_concurreny", param.getName());
    assertEquals("2", param.getSetting());
    assertEquals("unrecognized configuration parameter \"effective_io_concurreny\"",
        param.getError().orElseThrow());
    assertEquals(Optional.empty(), param.getHint());
  }

  @ParameterizedTest
  @ValueSource(strings = {"plpgsql.variable_conflict", "ongres._config4j$", "слоны.Ява"})
  void testCustomOptionValid(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter(input, "test");
    assertTrue(param.isValid());
  }

  @ParameterizedTest
  @ValueSource(strings = {"ongres._conf*ig4j$", ".demo"})
  void testCustomOptionInvalid(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    PgParameter param = validator.parameter(input, "on");
    assertFalse(param.isValid());
    assertEquals("unrecognized configuration parameter \"" + input + "\"",
        param.getError().orElseThrow());
  }

  @ParameterizedTest
  @NullSource
  void testNullVersion(String input) {
    NullPointerException e = assertThrows(NullPointerException.class,
        () -> GucValidator.forVersion(input));
    assertEquals("PostgreSQL version is required", e.getMessage());
  }

  @ParameterizedTest
  @NullSource
  void testNullParameter(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    NullPointerException e = assertThrows(NullPointerException.class,
        () -> validator.parameter(input, ""));
    assertEquals("parameter name is required", e.getMessage());
  }

  @ParameterizedTest
  @NullSource
  void testNullSetting(String input) {
    GucValidator validator = GucValidator.forVersion("14");
    NullPointerException e = assertThrows(NullPointerException.class,
        () -> validator.parameter("", input));
    assertEquals("setting value is required", e.getMessage());
  }

  @ParameterizedTest
  @ValueSource(strings = {"TIMEZONE", "TimeZone", "timezone", "tImEzOnE"})
  void testPreserveOriginalCaseTimeZone(String input) {
    GucValidator validator = GucValidator.forVersion("9.6");
    PgParameter parameter = validator.parameter(input, "UTC-6");
    assertEquals("TimeZone", parameter.getName());
    assertEquals("UTC-6", parameter.getSetting());
  }

  @ParameterizedTest
  @ValueSource(strings = {"DATESTYLE", "DateStyle", "datestyle", "DaTeStYlE"})
  void testPreserveOriginalCaseDateStyle(String input) {
    GucValidator validator = GucValidator.forVersion("9.6");
    PgParameter parameter = validator.parameter(input, "European");
    assertEquals("DateStyle", parameter.getName());
    assertEquals("European", parameter.getSetting());
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/guc-info/params-V13.csv", delimiter = '\t')
  void testListOfAllParametersForVersion13(String name, String vartype, String defValue,
      String defUnit) {
    GucValidator validator = assertDoesNotThrow(() -> GucValidator.forVersion("13"));

    // Clean nulls from default value
    final String setting = defValue == null || "\\N".equals(defValue) ? "" : defValue;
    PgParameter parameter = assertDoesNotThrow(() -> validator.parameter(name, setting));
    assertTrue(parameter.isValid(), () -> "Parameter " + name + " is not valid");
    assertEquals(name, parameter.getName());

    // Skip settings with units as we don't get conversions from CSV
    // and also skip parameters with special octal format.
    final String unit = "\\N".equals(defUnit) ? null : defUnit;
    List<String> octalFormat = Arrays.asList("unix_socket_permissions", "log_file_mode");
    if (unit == null && !octalFormat.contains(name)) {
      assertEquals(setting, parameter.getSetting());
    }
  }

  @ParameterizedTest
  @CsvFileSource(resources = "/guc-info/params-V14.csv", delimiter = '\t')
  void testListOfAllParametersForVersion14(String name, String vartype, String defValue,
      String defUnit) {
    GucValidator validator = assertDoesNotThrow(() -> GucValidator.forVersion("14"));

    // Clean nulls from default value
    final String setting = defValue == null || "\\N".equals(defValue) ? "" : defValue;
    PgParameter parameter = assertDoesNotThrow(() -> validator.parameter(name, setting));
    assertTrue(parameter.isValid(), () -> "Parameter " + name + " is not valid");
    assertEquals(name, parameter.getName());

    // Skip settings with units as we don't get conversions from CSV
    // and also skip parameters with special octal format.
    final String unit = "\\N".equals(defUnit) ? null : defUnit;
    List<String> octalFormat = Arrays.asList("unix_socket_permissions", "log_file_mode");
    if (unit == null && !octalFormat.contains(name)) {
      assertEquals(setting, parameter.getSetting());
    }
  }

  @ParameterizedTest
  @CsvSource({"-1,-1", "0,0kB", "1,8kB", "1,8kB", "2,16kB", "3,24kB", "4,32kB"})
  void testWalBuffers(String input, String expected) {
    GucValidator validator = GucValidator.forVersion("9.6");
    PgParameter parameter = validator.parameter("wal_buffers", input);
    assertEquals(expected, parameter.getSetting());
  }

  @ParameterizedTest
  @ValueSource(strings = {"autovacuum_vacuum_cost_delay", "autovacuum_work_mem",
      "log_autovacuum_min_duration", "log_min_duration_statement", "log_temp_files",
      "max_standby_archive_delay", "max_standby_streaming_delay", "old_snapshot_threshold",
      "temp_file_limit"})
  void testMinusOneParametersAsValid(String input) {
    GucValidator validator = GucValidator.forVersion("13");
    PgParameter parameter = validator.parameter(input, "-1");
    assertTrue(parameter.isValid());
    assertEquals(input, parameter.getName());
    assertEquals("-1", parameter.getSetting());
  }

  @ParameterizedTest
  @CsvSource({"-1,-1", "1.79769e+308,1.79769e+308", "123E-15,1.23e-13",
      "0.000000000000123,1.23e-13"})
  void testJitAboveCost(String input, String expected) {
    GucValidator validator = GucValidator.forVersion("11");
    PgParameter parameter = validator.parameter("jit_above_cost", input);
    assertTrue(parameter.isValid());
    assertEquals("jit_above_cost", parameter.getName());
    assertEquals(expected, parameter.getSetting());
  }

  @ParameterizedTest
  @CsvSource({"0.1,0", "0.5,0", "0.6,1", "10.7,11", "12.59,13", "123E-15,0", "0.000000000000123,0"})
  void testIntegerWithRealValue(String input, String expected) {
    GucValidator validator = GucValidator.forVersion("11");
    PgParameter parameter = validator.parameter("tcp_keepalives_count", input);
    assertTrue(parameter.isValid());
    assertEquals("tcp_keepalives_count", parameter.getName());
    assertEquals(expected, parameter.getSetting());
  }

  @Test
  void testWithExponentialValue() {
    GucValidator validator = GucValidator.forVersion("13");
    PgParameter parameter = validator.parameter("deadlock_timeout", "2.1474836e+09us");
    assertTrue(parameter.isValid());
    assertEquals("deadlock_timeout", parameter.getName());
    assertEquals("2147484ms", parameter.getSetting());
  }

  @Test
  void testWithDecimalValue() {
    GucValidator validator = GucValidator.forVersion("13");
    PgParameter parameter = validator.parameter("vacuum_cost_delay", "99.99ms");
    assertTrue(parameter.isValid());
    assertEquals("vacuum_cost_delay", parameter.getName());
    assertEquals("99990us", parameter.getSetting());
  }

  @Test
  void postgresql16Parameter() {
    GucValidator validator = GucValidator.forVersion("16");
    PgParameter parameter = validator.parameter("vacuum_buffer_usage_limit", "16777216");
    assertTrue(parameter.isValid());
    assertEquals("vacuum_buffer_usage_limit", parameter.getName());
    assertEquals("16GB", parameter.getSetting());
  }

  @Test
  void postgresql17Parameter() {
    GucValidator validator = GucValidator.forVersion("17");
    PgParameter parameter = validator.parameter("wal_summary_keep_time", "7200min");
    assertTrue(parameter.isValid());
    assertEquals("wal_summary_keep_time", parameter.getName());
    assertEquals("5d", parameter.getSetting());
  }

}
