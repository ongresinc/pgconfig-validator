/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;

class MinMaxTest {

  @Test
  void testIsValid() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("cpu_tuple_cost")
        .type(VarType.REAL)
        .minValue(new BigDecimal("0"))
        .maxValue(new BigDecimal("1.79769e+308"))
        .addEnumValues(new String[0])
        .build();

    MinMaxValidator valid = new MinMaxValidator(new BigDecimal("0.01"), setting);
    assertTrue(valid.isValid());

    MinMaxValidator invalid = new MinMaxValidator(new BigDecimal("-0.01"), setting);
    assertFalse(invalid.isValid());
  }

  @Test
  void testGetErrorMessageWithUnit() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("temp_buffers")
        .type(VarType.INTEGER)
        .defaultUnit(Unit.KB_8)
        .minValue(new BigDecimal("100"))
        .maxValue(new BigDecimal("1073741823"))
        .addEnumValues(new String[0])
        .build();

    MinMaxValidator valid = new MinMaxValidator(new BigDecimal("4096"), setting);
    assertEquals(Optional.empty(), valid.getErrorMessage());

    MinMaxValidator invalidMin = new MinMaxValidator(new BigDecimal("50"), setting);
    assertEquals(
        "50 8kB is outside the valid range for parameter \"temp_buffers\" (100 .. 1073741823)",
        invalidMin.getErrorMessage().orElseThrow());

    MinMaxValidator invalidMax = new MinMaxValidator(new BigDecimal("1073741824"), setting);
    assertEquals(
        "1073741824 8kB is outside the valid range for parameter "
            + "\"temp_buffers\" (100 .. 1073741823)",
        invalidMax.getErrorMessage().orElseThrow());
  }

  @Test
  void testGetErrorMessageWithoutUnit() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("geqo_seed")
        .type(VarType.REAL)
        .minValue(new BigDecimal("0"))
        .maxValue(new BigDecimal("1"))
        .addEnumValues(new String[0])
        .build();

    MinMaxValidator valid = new MinMaxValidator(new BigDecimal("0.01"), setting);
    assertEquals(Optional.empty(), valid.getErrorMessage());

    MinMaxValidator invalid = new MinMaxValidator(new BigDecimal("-0.01"), setting);
    assertEquals("-0.01 is outside the valid range for parameter \"geqo_seed\" (0 .. 1)",
        invalid.getErrorMessage().orElseThrow());
  }

  @Test
  void testInvalidType() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("demo")
        .type(VarType.BOOL)
        .addEnumValues(new String[0])
        .build();

    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> new MinMaxValidator(BigDecimal.ONE, setting));
    assertEquals("Parameter must be of type integer or real", e.getMessage());
  }

}
