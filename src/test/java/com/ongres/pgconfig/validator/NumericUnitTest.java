/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

class NumericUnitTest {

  @Test
  void testDefaultUnit() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("128")
        .defaultUnit(Unit.KB)
        .version(PgVersion.V10)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("128"), num.getValue());
    assertEquals(Unit.KB, num.getUnit());
    assertEquals("128kB", num.getSetting());
  }

  @Test
  void test16MbUnit() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("128")
        .defaultUnit(Unit.MB_16)
        .version(PgVersion.V10)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("128"), num.getValue());
    assertEquals(Unit.MB_16, num.getUnit());
    assertEquals("2GB", num.getSetting());
  }

  @Test
  void testBUnit() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("8kB")
        .defaultUnit(Unit.B)
        .version(PgVersion.V12)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("8192"), num.getValue());
    assertEquals(Unit.B, num.getUnit());
    assertEquals("8kB", num.getSetting());
  }

  @Test
  void testUndivisibleBUnit() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("4095")
        .defaultUnit(Unit.B)
        .version(PgVersion.V12)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("4095"), num.getValue());
    assertEquals(Unit.B, num.getUnit());
    assertEquals("4095B", num.getSetting());
  }

  @Test
  void test512MB() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("512 MB")
        .defaultUnit(Unit.KB_8)
        .version(PgVersion.V10)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("65536"), num.getValue());
    assertEquals(Unit.KB_8, num.getUnit());
    assertEquals("512MB", num.getSetting());
  }

  @Test
  void test4GB() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("4 GB")
        .version(PgVersion.V10)
        .defaultUnit(Unit.KB_8)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("524288"), num.getValue());
    assertEquals(Unit.KB_8, num.getUnit());
    assertEquals("4GB", num.getSetting());
  }

  @Test
  void test4096GB() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("4096 GB")
        .version(PgVersion.V10)
        .defaultUnit(Unit.KB)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("4294967296"), num.getValue());
    assertEquals(Unit.KB, num.getUnit());
    assertEquals("4TB", num.getSetting());

    NumericUnit numPg93 = UnitParserBuilder.builder()
        .value("4096 GB")
        .version(PgVersion.V9_3)
        .defaultUnit(Unit.KB)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("4294967296"), numPg93.getValue());
    assertEquals(Unit.KB, numPg93.getUnit());
    assertEquals("4096GB", numPg93.getSetting());
  }

  @Test
  void testBigBytes() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("1099511627776B")
        .version(PgVersion.V11)
        .defaultUnit(Unit.MB)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("1048576"), num.getValue());
    assertEquals(Unit.MB, num.getUnit());
    assertEquals("1TB", num.getSetting());

    NumericUnit numPg93 = UnitParserBuilder.builder()
        .value("1099511627776B")
        .version(PgVersion.V12)
        .defaultUnit(Unit.KB)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("1073741824"), numPg93.getValue());
    assertEquals(Unit.KB, numPg93.getUnit());
    assertEquals("1TB", numPg93.getSetting());
  }

  @Test
  void testInvalidUnitForPostgresVersion() {
    try {
      UnitParserBuilder.builder()
          .value("1099511627776B")
          .version(PgVersion.V10)
          .defaultUnit(Unit.MB)
          .type(VarType.INTEGER)
          .build();
    } catch (IllegalArgumentException e) {
      assertEquals("Valid units for this parameter are \"kB\", \"MB\", \"GB\", \"TB\".",
          e.getMessage());
    }
  }

  @Test
  void test600min() {
    NumericUnit num = UnitParserBuilder.builder()
        .version(PgVersion.V10)
        .value("600min")
        .defaultUnit(Unit.MS)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("36000000"), num.getValue());
    assertEquals(Unit.MS, num.getUnit());
    assertEquals("10h", num.getSetting());
  }

  @Test
  void testFractionalUnit() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("0.05s")
        .defaultUnit(Unit.MS)
        .version(PgVersion.V10)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("50"), num.getValue());
    assertEquals(Unit.MS, num.getUnit());
    assertEquals("50ms", num.getSetting());
  }

  @Test
  void testMicrosecondsDefaultUnit() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("0.5s")
        .defaultUnit(Unit.US)
        .version(PgVersion.V10)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("500000"), num.getValue());
    assertEquals(Unit.US, num.getUnit());
    assertEquals("500ms", num.getSetting());
  }

  @Test
  void testMicrosecondsDefaultUnitUndivisible() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("50")
        .defaultUnit(Unit.US)
        .version(PgVersion.V10)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("50"), num.getValue());
    assertEquals(Unit.US, num.getUnit());
    assertEquals("50us", num.getSetting());
  }

  @Test
  void testMinutesDefaultUnitUndivisible() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("50")
        .defaultUnit(Unit.MIN)
        .version(PgVersion.V10)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("50"), num.getValue());
    assertEquals(Unit.MIN, num.getUnit());
    assertEquals("50min", num.getSetting());
  }

  @Test
  void testSecondsDefaultUnitUndivisible() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("1234s")
        .defaultUnit(Unit.S)
        .version(PgVersion.V10)
        .type(VarType.INTEGER)
        .build();
    assertEquals(new BigDecimal("1234"), num.getValue());
    assertEquals(Unit.S, num.getUnit());
    assertEquals("1234s", num.getSetting());
  }

  @Test
  void testUsSeconds() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("0.05us")
        .defaultUnit(Unit.US)
        .version(PgVersion.V12)
        .type(VarType.REAL)
        .build();
    assertEquals(new BigDecimal("0.05"), num.getValue());
    assertEquals(Unit.US, num.getUnit());
    assertEquals("0.05us", num.getSetting());
  }

  @Test
  void testMultipleUnits1() {
    try {
      UnitParserBuilder.builder()
          .value("1234 min s")
          .defaultUnit(Unit.S)
          .version(PgVersion.V10)
          .type(VarType.REAL)
          .build();
    } catch (IllegalArgumentException e) {
      assertEquals("Valid units for this parameter are \"ms\", \"s\", \"min\", \"h\", \"d\".",
          e.getMessage());
    }
  }

  @Test
  void testMultipleUnits2() {
    try {
      UnitParserBuilder.builder()
          .value("1234 s min")
          .defaultUnit(Unit.S)
          .version(PgVersion.V10)
          .type(VarType.REAL)
          .build();
    } catch (IllegalArgumentException e) {
      assertEquals("Valid units for this parameter are \"ms\", \"s\", \"min\", \"h\", \"d\".",
          e.getMessage());
    }
  }

  @Test
  void testExponentialUnit() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("1e+10d")
        .defaultUnit(Unit.MS)
        .version(PgVersion.V10)
        .type(VarType.REAL)
        .build();
    assertEquals(new BigDecimal("864000000000000000"), num.getValue());
    assertEquals(Unit.MS, num.getUnit());
    assertEquals("10000000000d", num.getSetting());
  }

  @Test
  void testDecimalUnit() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("99.99ms")
        .defaultUnit(Unit.MS)
        .version(PgVersion.V10)
        .type(VarType.REAL)
        .build();
    assertEquals(new BigDecimal("99.99"), num.getValue());
    assertEquals(Unit.MS, num.getUnit());
    assertEquals("99990us", num.getSetting());
  }

  @Test
  void testInvalid() {
    assertThrows(IllegalArgumentException.class,
        () -> UnitParserBuilder.builder()
            .value("X min")
            .defaultUnit(Unit.MS)
            .version(PgVersion.V10)
            .type(VarType.REAL)
            .build());
  }

  @Test
  void testInvalid2() {
    assertThrows(IllegalArgumentException.class,
        () -> UnitParserBuilder.builder()
            .value("MB100")
            .defaultUnit(Unit.KB)
            .version(PgVersion.V10)
            .type(VarType.REAL)
            .build());
  }

  @Test
  void testInvalid3() {
    assertThrows(IllegalArgumentException.class,
        () -> UnitParserBuilder.builder()
            .value("100MB100")
            .defaultUnit(Unit.KB)
            .version(PgVersion.V10)
            .type(VarType.REAL)
            .build());
  }

  @Test
  void testMinusOne() {
    NumericUnit num = UnitParserBuilder.builder()
        .value("-1")
        .defaultUnit(Unit.KB_8)
        .version(PgVersion.V10)
        .type(VarType.REAL)
        .build();
    assertEquals(new BigDecimal("-1"), num.getValue());
    assertEquals(Unit.KB_8, num.getUnit());
    assertEquals("-1", num.getSetting());
  }

}
