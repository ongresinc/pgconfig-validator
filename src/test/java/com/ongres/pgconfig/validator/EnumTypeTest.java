/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

class EnumTypeTest {

  @Test
  void testValid() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("synchronous_commit")
        .type(VarType.ENUM)
        .addEnumValues(new String[] {"local", "remote_write", "remote_apply", "on", "off"})
        .build();

    EnumValidator val = new EnumValidator("Remote_ApplY", setting);
    assertTrue(val.isValid());
    assertEquals("remote_apply", val.getSetting());
    assertEquals(Optional.empty(), val.getErrorMessage());
  }

  @Test
  void testInvalid() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("synchronous_commit")
        .type(VarType.ENUM)
        .addEnumValues(new String[] {"local", "remote_write", "remote_apply", "on", "off"})
        .build();

    EnumValidator val = new EnumValidator("heLLo", setting);
    assertFalse(val.isValid());
    assertEquals("heLLo", val.getSetting());
    assertEquals("invalid value for parameter \"synchronous_commit\": \"heLLo\"",
        val.getErrorMessage().orElseThrow());
  }

  @Test
  void testInvalidType() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("demo")
        .type(VarType.STRING)
        .addEnumValues(new String[] {"on", "off"})
        .build();

    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> new EnumValidator("hello", setting));
    assertEquals("Parameter must be of type enum", e.getMessage());
  }

}
