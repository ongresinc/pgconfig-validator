/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

class ReaderTest {

  @ParameterizedTest
  @EnumSource(PgVersion.class)
  void testAllVersion(PgVersion version) {
    Map<String, PgSetting> map = FlatFileReader.forVersion(version).toMap();
    assertNotNull(map);
    assertFalse(map.isEmpty());
    assertTrue(map.containsKey("autovacuum"));
    PgSetting param = map.get("autovacuum");
    assertNotNull(param);
    if (param != null) {
      VarType vartype = param.getType();
      assertEquals(VarType.BOOL, vartype);
    }
  }

  @ParameterizedTest
  @EnumSource(VarType.class)
  void testVarType(VarType type) {
    assertEquals(type, VarType.fromString(type.toString()),
        "VarType.toString() should match VarType.fromString()");
  }

  @Test
  void testInvalidVarTypeShouldThrow() {
    assertThrows(IllegalArgumentException.class, () -> VarType.fromString("x"));
  }

}
