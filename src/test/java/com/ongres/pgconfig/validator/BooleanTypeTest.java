/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

class BooleanTypeTest {

  @Test
  void testValid() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("demo")
        .type(VarType.BOOL)
        .addEnumValues(new String[0])
        .build();

    BooleanValidator val = new BooleanValidator("yes", setting);
    assertTrue(val.isValid());
    assertEquals(Optional.empty(), val.getErrorMessage());
  }

  @Test
  void testInvalid() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("demo")
        .type(VarType.BOOL)
        .addEnumValues(new String[0])
        .build();

    BooleanValidator val = new BooleanValidator("Hello", setting);
    assertFalse(val.isValid());
    assertEquals("parameter \"demo\" requires a Boolean value but was: \"Hello\"",
        val.getErrorMessage().orElseThrow());
  }

  @Test
  void testInvalidType() {
    PgSetting setting = ImmutablePgSetting.builder()
        .name("demo")
        .type(VarType.STRING)
        .addEnumValues(new String[0])
        .build();
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> new BooleanValidator("", setting));
    assertEquals("Parameter must be of type bool", e.getMessage());
  }

}
