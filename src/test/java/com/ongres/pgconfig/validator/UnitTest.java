/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class UnitTest {

  private static Stream<Arguments> unitConversions() {
    List<Arguments> args = new ArrayList<>();
    for (PgVersion ver : PgVersion.values()) {
      args.add(Arguments.of(Unit.GB, "30.1", Unit.KB, "31561728", ver));
      args.add(Arguments.of(Unit.GB, "30.1", Unit.MB, "30822", ver));
      args.add(Arguments.of(Unit.GB, "30.4", Unit.MB, "31130", ver));
      args.add(Arguments.of(Unit.GB, "30.5", Unit.MB, "31232", ver));
      args.add(Arguments.of(Unit.GB, "30.6", Unit.MB, "31334", ver));
      args.add(Arguments.of(Unit.GB, "30.9", Unit.MB, "31642", ver));
      args.add(Arguments.of(Unit.MB, "1024", Unit.KB, "1048576", ver));
      args.add(Arguments.of(Unit.D, "24.8", Unit.H, "595", ver));
      args.add(Arguments.of(Unit.H, "595", Unit.MS, "2142000000", ver));
      args.add(Arguments.of(Unit.H, "3", Unit.MIN, "180", ver));
      args.add(Arguments.of(Unit.D, "1.5", Unit.H, "36", ver));
      args.add(Arguments.of(Unit.H, "1.5", Unit.H, "1.5", ver));
      args.add(Arguments.of(Unit.S, "0.02388789", Unit.MS, "24", ver));

      if (ver.compareTo(Unit.B.getSinceVersion()) >= 0) {
        args.add(Arguments.of(Unit.KB, "30.1", Unit.B, "30822", ver));
        args.add(Arguments.of(Unit.MB, "30.1", Unit.B, "31561728", ver));
      }
      if (ver.compareTo(Unit.TB.getSinceVersion()) >= 0) {
        args.add(Arguments.of(Unit.TB, "2", Unit.GB, "2048", ver));
        args.add(Arguments.of(Unit.TB, "2", Unit.MB, "2097152", ver));
        args.add(Arguments.of(Unit.TB, "2", Unit.KB, "2147483648", ver));
      }
      if (ver.compareTo(Unit.US.getSinceVersion()) >= 0) {
        args.add(Arguments.of(Unit.D, "1.7", Unit.US, "147600000000", ver));
        args.add(Arguments.of(Unit.H, "1.7", Unit.US, "6120000000", ver));
        args.add(Arguments.of(Unit.MIN, "1.7", Unit.US, "102000000", ver));
        args.add(Arguments.of(Unit.S, "3", Unit.US, "3000000", ver));
        args.add(Arguments.of(Unit.US, "987.5688789", Unit.MS, "0.987569", ver));
      }
    }
    return args.stream();
  }

  @ParameterizedTest
  @MethodSource("unitConversions")
  void testConversionsUnits(Unit unitFrom, String valueFrom, Unit unitTo,
      String valueTo, PgVersion version) {
    BigDecimal val = unitFrom.convert(new BigDecimal(valueFrom), unitTo, version);
    assertEquals(valueTo, val.toPlainString(),
        () -> "Conversion from <" + valueFrom + " " + unitFrom.toString() + "> to: <"
            + unitTo.toString() + ">");
    assertEquals(0, new BigDecimal(valueTo).compareTo(val));
  }

  private static Stream<Arguments> illegalConversion() {
    return Stream.of(
        Arguments.of(Unit.TB, Unit.B, PgVersion.V9_1, true),
        Arguments.of(Unit.B, Unit.TB, PgVersion.V9_2, true),
        Arguments.of(Unit.B, Unit.MB, PgVersion.V9_2, true),
        Arguments.of(Unit.B, Unit.B, PgVersion.V10, true),
        Arguments.of(Unit.TB, Unit.TB, PgVersion.V9_3, true),
        Arguments.of(Unit.US, Unit.S, PgVersion.V11, true),
        Arguments.of(Unit.MB, Unit.B, PgVersion.V9_2, false),
        Arguments.of(Unit.MB, Unit.TB, PgVersion.V9_2, false),
        Arguments.of(Unit.S, Unit.US, PgVersion.V11, false));
  }

  @ParameterizedTest
  @MethodSource("illegalConversion")
  void testIllegalArgumentException(Unit from, Unit to, PgVersion version, boolean isSource) {
    IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
        () -> from.convert(BigDecimal.TEN, to, version));
    if (isSource) {
      assertEquals(
          "Source unit \"" + from + "\" is not compatible with version \"" + version + "\"",
          ex.getMessage());
    } else {
      assertEquals(
          "Destination unit \"" + to + "\" is not compatible with version \"" + version + "\"",
          ex.getMessage());
    }
  }

  private static Stream<Arguments> illegalConversionNotCompatible() {
    return Stream.of(
        Arguments.of(Unit.TB, Unit.D),
        Arguments.of(Unit.GB, Unit.H),
        Arguments.of(Unit.MB, Unit.MIN),
        Arguments.of(Unit.KB, Unit.S),
        Arguments.of(Unit.B, Unit.MS),
        Arguments.of(Unit.US, Unit.TB),
        Arguments.of(Unit.MIN, Unit.B),
        Arguments.of(Unit.D, Unit.B));
  }

  @ParameterizedTest
  @MethodSource("illegalConversionNotCompatible")
  void testSourceNotCompatibleWithDestination(Unit from, Unit to) {
    IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
        () -> from.convert(BigDecimal.TEN, to));
    assertEquals(
        "Source unit \"" + from + "\" is not compatible with destination unit \"" + to + "\"",
        ex.getMessage());
  }

  @ParameterizedTest
  @EnumSource(Unit.class)
  void testFromString(Unit unit) {
    assertEquals(unit, Unit.fromString(unit.toString()));
  }

  @Test
  void testValidMemoryUnits() {
    assertArrayEquals(new String[] {"kB", "MB", "GB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V9_1));
    assertArrayEquals(new String[] {"kB", "MB", "GB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V9_2));
    assertArrayEquals(new String[] {"kB", "MB", "GB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V9_3));
    assertArrayEquals(new String[] {"kB", "MB", "GB", "TB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V9_4));
    assertArrayEquals(new String[] {"kB", "MB", "GB", "TB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V9_5));
    assertArrayEquals(new String[] {"kB", "MB", "GB", "TB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V9_6));
    assertArrayEquals(new String[] {"kB", "MB", "GB", "TB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V10));
    assertArrayEquals(new String[] {"B", "kB", "MB", "GB", "TB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V11));
    assertArrayEquals(new String[] {"B", "kB", "MB", "GB", "TB"},
        Unit.validUnits(VarSubType.MEMORY, PgVersion.V12));
  }

  @Test
  void testValidTimeUnits() {
    assertArrayEquals(new String[] {"ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V9_1));
    assertArrayEquals(new String[] {"ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V9_2));
    assertArrayEquals(new String[] {"ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V9_3));
    assertArrayEquals(new String[] {"ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V9_4));
    assertArrayEquals(new String[] {"ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V9_5));
    assertArrayEquals(new String[] {"ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V9_6));
    assertArrayEquals(new String[] {"ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V10));
    assertArrayEquals(new String[] {"ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V11));
    assertArrayEquals(new String[] {"us", "ms", "s", "min", "h", "d"},
        Unit.validUnits(VarSubType.TIME, PgVersion.V12));
  }

  @ParameterizedTest
  @NullAndEmptySource
  @ValueSource(strings = {"  ", "none", "X", "123"})
  void testInvalidUnit(String source) {
    assertEquals(null, Unit.fromString(source));
  }

}
