/*
 * Copyright (C) 2020 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package com.ongres.pgconfig.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;

class PgSettingTest {

  @Test
  void testPgParameter() {
    PgParameter param = ImmutablePgParameter.builder()
        .name("bgwriter_flush_after")
        .setting("-1")
        .error("must be greater than or equal to 0")
        .build();

    assertNotNull(param);
    assertEquals("bgwriter_flush_after", param.getName());
    assertEquals(false, param.isValid());
    assertEquals("-1", param.getSetting());
    assertEquals("must be greater than or equal to 0", param.getError().orElseThrow());
  }

  @Test
  void testBuilderPgSetting() {
    PgSetting param = ImmutablePgSetting.builder()
        .name("bgwriter_flush_after")
        .type(VarType.fromString("integer"))
        .defaultUnit(Optional.ofNullable(Unit.fromString("8kB")))
        .defaultValue("64")
        .minValue(new BigDecimal(0))
        .maxValue(new BigDecimal(256))
        .addEnumValues(new String[0])
        .build();

    assertNotNull(param);
    assertEquals("bgwriter_flush_after", param.getName());
    assertEquals(VarType.INTEGER, param.getType());
    assertEquals(VarSubType.MEMORY, param.getDefaultUnit().orElseThrow().getSubType());
    assertEquals(Unit.KB_8, param.getDefaultUnit().orElseThrow());
    assertEquals("64", param.getDefaultValue().orElseThrow());
    assertEquals(new BigDecimal(0), param.getMinValue().orElseThrow());
    assertEquals(new BigDecimal(256), param.getMaxValue().orElseThrow());
    assertNotNull(param.getEnumValues());
  }

  @Test
  void testMaxValueMustBePresent() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("bgwriter_flush_after")
            .type(VarType.fromString("integer"))
            .minValue(new BigDecimal(0))
            .addEnumValues(new String[0])
            .build());
    assertEquals("Minimum and maximum value must be present",
        e.getMessage());
  }

  @Test
  void testMinValueMustBePresent() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("bgwriter_flush_after")
            .type(VarType.fromString("integer"))
            .maxValue(new BigDecimal(256))
            .addEnumValues(new String[0])
            .build());
    assertEquals("Minimum and maximum value must be present",
        e.getMessage());
  }

  @Test
  void testTypeMustBeNumericForMemoryUnit() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("work_mem")
            .type(VarType.fromString("bool"))
            .defaultUnit(Unit.KB)
            .addEnumValues(new String[0])
            .build());
    assertEquals("Type must be integer or floating point for unit \"kB\"",
        e.getMessage());
  }

  @Test
  void testTypeMustBeNumericForTimeUnit() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("vacuum_cost_delay")
            .type(VarType.fromString("enum"))
            .defaultUnit(Unit.MS)
            .addEnumValues(new String[0])
            .build());
    assertEquals("Type must be integer or floating point for unit \"ms\"",
        e.getMessage());
  }

  @Test
  void testEnumMustHaveValues() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("vacuum_cost_delay")
            .type(VarType.ENUM)
            .addEnumValues(new String[0])
            .build());
    assertEquals("Enumerated-type parameter must have values",
        e.getMessage());
  }

  @Test
  void testMinMaxMustBeNumeric() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("bgwriter_flush_after")
            .type(VarType.BOOL)
            .defaultValue("64")
            .minValue(new BigDecimal(0))
            .maxValue(new BigDecimal(256))
            .addEnumValues(new String[0])
            .build());
    assertEquals("Type must be integer or floating point",
        e.getMessage());
  }

  @Test
  void testMaxValueGreaterThanMinValue() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("bgwriter_flush_after")
            .type(VarType.INTEGER)
            .minValue(new BigDecimal(256))
            .maxValue(new BigDecimal(64))
            .addEnumValues(new String[0])
            .build());
    assertEquals("Maximum value must be greater than minimum value",
        e.getMessage());
  }

  @Test
  void testReader91() {
    Map<String, PgSetting> v91 = FlatFileReader.forVersion(PgVersion.V9_1).toMap();
    assertNotNull(v91);
    PgSetting param = v91.get("application_name");
    assertNotNull(param);
    if (param != null) {
      assertEquals("application_name", param.getName());
      assertEquals("", param.getDefaultValue().orElseThrow());
    }
    assertNotNull(v91.get("custom_variable_classes"));
  }

  @Test
  void testReader92() {
    Map<String, PgSetting> v92 = FlatFileReader.forVersion(PgVersion.V9_2).toMap();
    assertNotNull(v92);
    PgSetting param = v92.get("log_error_verbosity");
    assertNotNull(param);
    if (param != null) {
      assertEquals("log_error_verbosity", param.getName());
      assertEquals("default", param.getDefaultValue().orElseThrow());
      assertEquals(Arrays.asList("terse", "default", "verbose"),
          param.getEnumValues());
    }
    assertNull(v92.get("custom_variable_classes"));
  }

  @Test
  void testReader93() {
    Map<String, PgSetting> v93 = FlatFileReader.forVersion(PgVersion.V9_3).toMap();
    assertNotNull(v93);
    PgSetting param = v93.get("temp_buffers");
    assertNotNull(param);
    if (param != null) {
      assertEquals("temp_buffers", param.getName());
      assertEquals("integer", param.getType().toString());
      assertEquals("memory", param.getDefaultUnit().orElseThrow().getSubType().toString());
      assertEquals("8kB", param.getDefaultUnit().orElseThrow().toString());
      assertEquals("1024", param.getDefaultValue().orElseThrow());
      assertEquals("100", param.getMinValue().orElseThrow().toPlainString());
      assertEquals("1073741823", param.getMaxValue().orElseThrow().toPlainString());
      assertEquals(Arrays.asList(), param.getEnumValues());
    }
  }

  @Test
  void testReader94() {
    Map<String, PgSetting> v94 = FlatFileReader.forVersion(PgVersion.V9_4).toMap();
    assertNotNull(v94);
    PgSetting param = v94.get("default_transaction_isolation");
    assertNotNull(param);
    if (param != null) {
      assertEquals("default_transaction_isolation", param.getName());
      assertEquals(VarType.ENUM, param.getType());
      assertEquals(Optional.empty(), param.getDefaultUnit());
      assertEquals("read committed", param.getDefaultValue().orElseThrow());
      assertEquals(
          Arrays.asList("serializable", "repeatable read", "read committed", "read uncommitted"),
          param.getEnumValues());
    }
  }

  @Test
  void testReader95() {
    Map<String, PgSetting> v95 = FlatFileReader.forVersion(PgVersion.V9_5).toMap();
    assertNotNull(v95);
    PgSetting param = v95.get("ssl_ecdh_curve");
    assertNotNull(param);
    if (param != null) {
      assertEquals("ssl_ecdh_curve", param.getName());
      assertEquals("string", param.getType().toString());
      assertEquals("prime256v1", param.getDefaultValue().orElseThrow());
    }
  }

  @Test
  void testReader96() {
    Map<String, PgSetting> v96 = FlatFileReader.forVersion(PgVersion.V9_6).toMap();
    assertNotNull(v96);
    PgSetting param = v96.get("force_parallel_mode");
    assertNotNull(param);
    if (param != null) {
      assertEquals("force_parallel_mode", param.getName());
      assertEquals("enum", param.getType().toString());
      assertEquals("off", param.getDefaultValue().orElseThrow());
      assertEquals(Arrays.asList("off", "on", "regress"),
          param.getEnumValues());
    }
  }

  @Test
  void testReader10() {
    Map<String, PgSetting> v10 = FlatFileReader.forVersion(PgVersion.V10).toMap();
    assertNotNull(v10);
    PgSetting param = v10.get("vacuum_cost_delay");
    assertNotNull(param);
    if (param != null) {
      assertEquals("vacuum_cost_delay", param.getName());
      assertEquals(VarType.INTEGER, param.getType());
      assertEquals(VarSubType.TIME, param.getDefaultUnit().orElseThrow().getSubType());
      assertEquals("0", param.getDefaultValue().orElseThrow());
      assertEquals(Unit.MS, param.getDefaultUnit().orElseThrow());
    }
  }

  @Test
  void testReader11() {
    Map<String, PgSetting> v11 = FlatFileReader.forVersion(PgVersion.V11).toMap();
    assertNotNull(v11);
    PgSetting param = v11.get("enable_parallel_hash");
    assertNotNull(param);
    if (param != null) {
      assertEquals("enable_parallel_hash", param.getName());
      assertEquals(VarType.BOOL, param.getType());
      assertEquals("on", param.getDefaultValue().orElseThrow());
    }
  }

  @Test
  void testReader12() {
    Map<String, PgSetting> v12 = FlatFileReader.forVersion(PgVersion.V12).toMap();
    assertNotNull(v12);
    PgSetting param = v12.get("vacuum_cost_delay");
    assertNotNull(param);
    if (param != null) {
      assertEquals("vacuum_cost_delay", param.getName());
      assertEquals(VarType.REAL, param.getType());
      assertEquals(VarSubType.TIME, param.getDefaultUnit().orElseThrow().getSubType());
      assertEquals("0", param.getDefaultValue().orElseThrow());
      assertEquals(Unit.MS, param.getDefaultUnit().orElseThrow());
    }
  }

  @Test
  void testMinValueMustNotBePresent() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("demo")
            .type(VarType.BOOL)
            .minValue(new BigDecimal(10))
            .addEnumValues(new String[0])
            .build());
    assertEquals("Type must be integer or floating point",
        e.getMessage());
  }

  @Test
  void testMaxValueMustNotBePresent() {
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ImmutablePgSetting.builder()
            .name("demo")
            .type(VarType.BOOL)
            .maxValue(new BigDecimal(10))
            .addEnumValues(new String[0])
            .build());
    assertEquals("Type must be integer or floating point",
        e.getMessage());
  }

}
