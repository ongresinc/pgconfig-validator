package test.pgconfig;

import com.ongres.pgconfig.validator.GucValidator;
import com.ongres.pgconfig.validator.PgParameter;

public class Main {
  public static void main(String... args) {
    GucValidator validator = GucValidator.forVersion("17");
    PgParameter param = validator.parameter("work_mem", "1048576 CB");
    System.out.println(param.getName()); // Parameter name
    System.out.println(param.getSetting()); // Parsed setting
    System.out.println(param.isValid()); // Parameter is valid
    System.out.println(param.getError()); // No error message
    System.out.println(param.getHint()); // No hint message
  }
}
