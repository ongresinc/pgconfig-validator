/*
 * Copyright (c) 2024 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

open module test.pgconfig {
    requires com.ongres.pgconfig.validator;

    requires transitive org.junit.jupiter.engine;
    requires transitive org.junit.jupiter.api;
    requires transitive org.junit.jupiter.params;
}