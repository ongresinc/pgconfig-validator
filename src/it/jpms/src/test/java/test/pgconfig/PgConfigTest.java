/*
 * Copyright (c) 2024 OnGres, Inc.
 * SPDX-License-Identifier: Apache-2.0
 */

package test.pgconfig;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.Arrays;

import com.ongres.pgconfig.validator.GucValidator;
import com.ongres.pgconfig.validator.PgParameter;
import org.junit.jupiter.api.Test;
import java.util.Optional;

class PgConfigTest {

  @Test
  void accessPublic() {
    assertEquals("com.ongres.pgconfig.validator", GucValidator.class.getModule().getName());
  }

  @Test
  void testRunningAsModule() {
    GucValidator validator = GucValidator.forVersion("17");
    PgParameter param = validator.parameter("work_mem", "1048576 MB");
    assertEquals("work_mem", param.getName()); // Parameter name
    assertEquals("1TB", param.getSetting()); // Parsed setting
    assertTrue(param.isValid()); // Parameter is valid
    assertEquals(Optional.empty(), param.getError()); // No error message
    assertEquals(Optional.empty(), param.getHint()); // No hint message
  }

}
